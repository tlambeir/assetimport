﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationControlsUI : MonoBehaviour {

    public Animator animator;
    private bool paused = false;

    public void pause()
    {
        Debug.Log("pauze");
        paused = !paused;
        if (paused)
        {
            animator.enabled = false;
            animator.speed = 0f;
        }
        else if (!paused)
        {
            animator.enabled = true;
            animator.speed = 1f;
        }
    }

    public void backward()
    {
        Debug.Log("rewind");
        animator.speed = -4f;
    }

    public void forward()
    {
        Debug.Log("forward");
        animator.speed = 4f;
    }
}
