﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class descriptionmanager : MonoBehaviour {

    public Text targetTitle;
    public TextMeshProUGUI targetDescription;
    public Text sourceTitle;
    public TextMeshProUGUI sourceDescription;
    public GameObject inspectButton;
    public string loadId = "";
    public SceneController sceneController;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        targetTitle.text = sourceTitle.text;
        targetDescription.text = sourceDescription.text;
        if (loadId != null && loadId != "" && !inspectButton.activeSelf)
        {
            inspectButton.SetActive(true);
            sceneController.loadId = loadId;
        }
        else if (loadId != null && loadId != "" && inspectButton && inspectButton.activeSelf) {
            sceneController.loadId = loadId;
        }
        else if ((loadId == null || loadId == "") && inspectButton && inspectButton.activeSelf)
        {
            inspectButton.SetActive(false);
        }
    }
}
