﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class landscapeTransformManager : MonoBehaviour {

    RectTransform rectTransform;
    Vector2 offsetMin;
    Vector2 offsetMax;
    public float target = 800f;


    void Start()
    {
        //Fetch the RectTransform from the GameObject
        rectTransform = GetComponent<RectTransform>();
        offsetMin = rectTransform.offsetMin;
        offsetMax = rectTransform.offsetMax;
    }

    void Update()
    {

        if (Screen.width < Screen.height)
        {
            rectTransform.offsetMin = offsetMin;
            rectTransform.offsetMax = offsetMax;
        }
        else {
            rectTransform.offsetMin = new Vector2(target, 0);
            rectTransform.offsetMax = new Vector2(target*-1f, -0);
        }
    }
}
