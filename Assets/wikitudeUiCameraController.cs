﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wikitudeUiCameraController : MonoBehaviour {

    public Camera mainCamera;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Camera uiCamera = gameObject.GetComponent<Camera>();
        CopyCameraSettings(mainCamera, uiCamera);
        CopyTransform(mainCamera.GetComponent<Transform>(), transform);
        uiCamera.depth = -1f;
        uiCamera.cullingMask = (1 << LayerMask.NameToLayer("UI"));
    }

    void CopyTransform(Transform original, Transform Destination) {
        Destination.position = original.position;
        Destination.rotation = original.rotation;
        Destination.localScale = original.localScale;
    }

    void CopyCameraSettings(Camera original, Camera Destination)
    {
        Destination.fieldOfView = original.fieldOfView;
    }
}
