﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public VirtualJoystick joystick;
    public float speed = 150.0f;
    public GameObject target;
    Vector3 originalPos;
    Vector3 originalRotation;
    private Rigidbody rigid;
    private bool shouldMove = true;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        originalPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        //originalRotation = new Vector3(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
        //alternatively, just: originalPos = gameObject.transform.position;

    }

    private void FixedUpdate()
    {
        if(shouldMove)
            transform.Translate(joystick.InputDirection * speed * Time.deltaTime);
        //rigid.AddForce(joystick.InputDirection * speed * Time.deltaTime);
        // rigid.MovePosition(joystick.InputDirection * speed * Time.deltaTime);
        // rigid.velocity = joystick.InputDirection * speed * Time.deltaTime;
    }
    private void OnCollisionEnter(Collision collision)
    {
        rigid.velocity = Vector3.zero;
        Debug.Log("is colliding");
        //shouldMove = false;
    }
    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("is exitting  Collision");
        //shouldMove = true;
    }

    public void setPosition(Vector3 targetPosition, Vector3 playerPosition) {
        Debug.Log("new position: " + targetPosition);
        target.transform.localPosition = targetPosition;
        target.GetComponent<TargetIndicator>().enabled = true;
        transform.localPosition = playerPosition;
    }

    public void OnResetPosition()
    {
        gameObject.transform.position = originalPos;
        // gameObject.transform.rotation = Quaternion.Euler(originalRotation);
    }
}
