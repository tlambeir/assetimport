﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    public Image bgImg, jsImg;
    public GameObject jsImgGameObject;
    public Vector3 InputDirection { set; get; }

    private RectTransform bgImgRectTransform;
    private RectTransform jsImgRectTransform;
    private void Start()
    {
        bgImg = GetComponent<Image>();
        jsImg = jsImgGameObject.GetComponent<Image>();
        // jsImg = GetComponentsInChildren<Image>()[1];
        bgImgRectTransform = GetComponent<RectTransform>();
        jsImgRectTransform = jsImgGameObject.GetComponent<RectTransform>();
        InputDirection = Vector3.zero;
    }

    //EventSystems interfaces
    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos = Vector2.zero;

        
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle
            (   bgImgRectTransform,
                ped.position,
                ped.pressEventCamera,
                out pos))
        {
            pos.x = (pos.x / bgImgRectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImgRectTransform.sizeDelta.y);

            float x = (bgImgRectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
            float y = (bgImgRectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

            InputDirection = new Vector3(x, 0, y);
            InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;
            jsImgRectTransform.anchoredPosition = new Vector3(InputDirection.x * (bgImgRectTransform.sizeDelta.x / 3)
                , InputDirection.z * (bgImgRectTransform.sizeDelta.y / 3));
        }
    }
    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }
    public virtual void OnPointerUp(PointerEventData ped)
    {
        InputDirection = Vector3.zero;
        jsImgRectTransform.anchoredPosition = Vector3.zero;
    }
}
