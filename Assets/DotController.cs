﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class DotController : MonoBehaviour {
    public GameObject parent;
    public GameObject child;
    public Transform parentTransform;
    public float size = 1;
    // Use this for initialization
    public bool isParent = false;
    public bool isActive = false;

    public void setSprite(bool isParent) {
        this.isParent = isParent;
        if (isParent)
        {
            parent.SetActive(true);
        }
        else {
            child.SetActive(true);
        }
    }
    // Update is called once per frame
    void Update () {
        LeanCameraZoomSmooth camzoom = Object.FindObjectOfType<LeanCameraZoomSmooth>();

        transform.localScale = new Vector3(size * 0.4f * camzoom.Zoom / 7, size*0.4f * camzoom.Zoom / 7, size*0.4f * camzoom.Zoom / 7);
        if(parentTransform != null)
        transform.position = new Vector3(parentTransform.position.x, parentTransform.position.y, parentTransform.position.z);
    }

    private void playActiveAnimation(GameObject item, string state) {
        Animator animator = item.GetComponent<Animator>();
        animator.Play(state);
    }

    private void clearActive() {
        DotController[] dots = FindObjectsOfType<DotController>();
        foreach (DotController dot in dots) {
            if (dot.isActive) {
                dot.isActive = false;
                if (dot.isParent)
                {
                    playActiveAnimation(dot.parent,"default");
                }
                else
                {
                    playActiveAnimation(dot.child,"default");
                }
            }
            
        }
    }

    public void setActive() {
        clearActive();
        isActive = true;
        if (isParent)
        {
            playActiveAnimation(parent, "Active");
        }
        else
        {
            playActiveAnimation(child, "Active");
        }
    }

    void OnCollisionEnter(Collision other)
    {
        DotController isDot = other.gameObject.GetComponent<DotController>();
        if (!isDot) {
            parent.SetActive(false);
            child.SetActive(false);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        setSprite(isParent);
    }
}
