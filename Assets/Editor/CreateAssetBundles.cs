﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreateAssetBundles
{
    [MenuItem ("Assets/Build AssetBundles")]
	static void BuildAllAssetBundles ()
	{

        //Build the asset bundle
        BuildPipeline.BuildAssetBundles ("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
	}

    [MenuItem("Assets/Build Scene Bundle")]
    static void BuildSceneBundle()
    {
        //Check which prefabs are in current scene, and save their paths
        List<string> assets = new List<string>();

        string basePrefabPath = "Assets/Prefabs";

        DirectoryInfo info = new DirectoryInfo(basePrefabPath);
        FileInfo[] files = info.GetFiles();
        GameObject[] objects = GameObject.FindObjectsOfType<GameObject>();

        for (int i = 0; i < objects.Length; i++)
            for (int j = 0; j < files.Length; j++)
                if (objects[i].name == Path.GetFileNameWithoutExtension(files[j].Name) && objects[i].transform.parent == null)
                {
                    assets.Add(basePrefabPath + "/" + files[j].Name);

                }
                

        //Get lightmaps paths from scene folder 
        string currentScene = SceneManager.GetActiveScene().name;
        string baseLightmapPath = "Assets/Scenes/" + currentScene;

        for (int i = 0; i < LightmapSettings.lightmaps.Length; i++)
        {

            string intensityMapPath = baseLightmapPath + "/" + "Lightmap-" + i + "_comp_light.exr";
            assets.Add(intensityMapPath);

            if (LightmapSettings.lightmapsMode == LightmapsMode.CombinedDirectional)
            {
                string directionMapPath = baseLightmapPath + "/" + "Lightmap-" + i + "_comp_dir.png";
                assets.Add(directionMapPath);
            }
            
        }
        //Add to an assetbundle build
        AssetBundleBuild[] abb = new AssetBundleBuild[1];
        abb[0].assetNames = assets.ToArray();
        abb[0].assetBundleName = currentScene + "AssetBundle";

        //Build the asset bundle
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", abb, BuildAssetBundleOptions.IgnoreTypeTreeChanges, BuildTarget.Android);
    }

}