﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationAdjuster : MonoBehaviour {

	// Use this for initialization
	void Start () {

        if (ApplicationModel.model == "airscale")
        {
            transform.rotation = Quaternion.Euler(-90, 0, 0);
        }

        else if (ApplicationModel.model == "IP-MPLS")
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
