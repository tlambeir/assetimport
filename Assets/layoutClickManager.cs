﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class layoutClickManager : MonoBehaviour {

    public LeftNav leftNav;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && !RectTransformUtility.RectangleContainsScreenPoint(
                 transform.GetComponent<RectTransform>(),
                 Input.mousePosition,
                 null))
        {
            Scene m_Scene = SceneManager.GetActiveScene();
            string scene = m_Scene.name;
            //clicked outside
            if (leftNav.isPaused == true && scene != "Inspect")
            {
                leftNav.slide();
            }
        }
        else if (Input.GetMouseButtonDown(0) && RectTransformUtility.RectangleContainsScreenPoint(
                 transform.GetComponent<RectTransform>(),
                 Input.mousePosition,
                 null)) {
            //clicked inside
        }
    }
}
