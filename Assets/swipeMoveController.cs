﻿using UnityEngine;
using UnityEngine.UI;

namespace Lean.Touch
{
    // This script will tell you which direction you swiped in
    public class swipeMoveController : MonoBehaviour
    {

        Rigidbody m_Rigidbody;
        public float m_Speed = 100.0f;

        protected virtual void OnEnable()
        {
            // Hook into the events we need
            LeanTouch.OnFingerSwipe += OnFingerSwipe;
        }

        protected virtual void OnDisable()
        {
            // Unhook the events
            LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        }

        public void OnFingerSwipe(LeanFinger finger)
        {
                // Store the swipe delta in a temp variable
                var swipe = finger.SwipeScreenDelta;

                if (swipe.x < -Mathf.Abs(swipe.y))
                {
                transform.Translate(Vector3.left * m_Speed);
                Debug.Log("You swiped left!");
                }

                if (swipe.x > Mathf.Abs(swipe.y))
                {
                transform.Translate(Vector3.right * m_Speed);
                Debug.Log("You swiped right!");
                }

                if (swipe.y < -Mathf.Abs(swipe.x))
                {
                Debug.Log("You swiped down!");
                transform.Translate(Vector3.back * m_Speed);
                }

                if (swipe.y > Mathf.Abs(swipe.x))
                {
                transform.Translate(Vector3.forward * m_Speed);
                Debug.Log("You swiped up!");
                }
        }
    }
}