﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SampleController : MonoBehaviour 
{
    public StepNavigation stepNavigation;
    public LeftNav LeftNav;
    public GameObject arAnimation;
    public GameObject targetLostOverlay;

    protected virtual void Start() {
		QualitySettings.shadowDistance = 60.0f;
	}
	
	public virtual void OnBackButtonClicked() {
		SceneManager.LoadScene("Main Menu");
	}

	// URLResource events
	public virtual void OnFinishLoading() {
		Debug.Log("URL Resource loaded successfully.");
	}

	public virtual void OnErrorLoading(int errorCode, string errorMessage) {
		Debug.LogError("Error loading URL Resource!\nErrorCode: " + errorCode + "\nErrorMessage: " + errorMessage);
	}

	// Tracker events
	public virtual void OnTargetsLoaded() {
		Debug.Log("Targets loaded successfully.");
	}

	public virtual void OnErrorLoadingTargets(int errorCode, string errorMessage) {
		Debug.LogError("Error loading targets!\nErrorCode: " + errorCode + "\nErrorMessage: " + errorMessage);
	}

	protected virtual void Update() {
		// Also handles the back button on Android
		if (Input.GetKeyDown(KeyCode.Escape)) { 
			OnBackButtonClicked();
		}
	}

    // Tracker events
    public virtual void OnImageRecognized()
    {
        if(targetLostOverlay)
            targetLostOverlay.SetActive(false);
        if (stepNavigation) {
            stepNavigation.animator = FindObjectOfType(typeof(Animator)) as Animator;
            if (stepNavigation.getCurrentIndex() > 0) {
                stepNavigation.startAnimation();
            }
        }
        if (arAnimation)
            arAnimation.SetActive(false);
        // reload lightmap here?
        LightmapParams.RestoreAll(LightmapsMode.CombinedDirectional, true, true);
    }

    public virtual void OnImageLost()
    {
        if(LeftNav)
        StartCoroutine(menuAnimations());
       
        if(targetLostOverlay)
        targetLostOverlay.SetActive(true);
        if (arAnimation) {
            arAnimation.SetActive(true);
            foreach (Animator animator in arAnimation.GetComponentsInChildren<Animator>())
                animator.SetTrigger("ar_entry");
        }        
    }

    IEnumerator menuAnimations()
    {
        LeftNav.hideDescription();
        yield return new WaitForSeconds(0.5f);
        if (LeftNav.isPaused)
            LeftNav.setActive(false);
    }

}
