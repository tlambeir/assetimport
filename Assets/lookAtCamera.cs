﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtCamera : MonoBehaviour {

    public Transform target;


    void Start()
    {
        target = Camera.main.gameObject.transform;
    }

    void Update()
    {
        // Rotate the camera every frame so it keeps looking at the target
        //transform.LookAt(target);
        transform.LookAt(2 * transform.position - target.position);
    }
}
