﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmapParam : MonoBehaviour {

	public int lightmapIndex;
	public Vector4 lightmapScaleOffset;
}
