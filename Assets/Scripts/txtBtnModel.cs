﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class txtBtnModel : MonoBehaviour {

	public GameObject icon;
	public Sprite iconSprite;
	public Text text;
	public Color titleColor;
	public Color itemColor;
	public Color highlightColor;
	private ColorBlock theColor;
	public Color originalColor;
    public DotController dot;
    public string index;

	void Start(){
		// GetComponent<HorizontalLayoutGroup> ().padding.left = 10;
		text.supportRichText = true;
	}

	public void setHighlight(){
		text.color = highlightColor;
	}

	public void resetColor(){
		text.color = originalColor;
	}

	public void setSprite( Sprite sprite, bool isBack = false, bool isTitle = false){
		text.color = itemColor;
		if (isTitle) {
			text.color = titleColor;
			theColor = GetComponent<Button>().colors;
//			theColor.highlightedColor = Color.blue;
//			theColor.normalColor = Color.cyan;
			theColor.pressedColor = Color.white;

			GetComponent<Button>().colors = theColor;
		}

			
		if (isBack) {
			RectTransform rt = icon.GetComponent<RectTransform> ();
			rt.sizeDelta = new Vector2 (36.8f, 88f);
		}
		if (sprite != null) {
			icon.SetActive (true);
            icon.GetComponent<UnityEngine.UI.Image> ().overrideSprite = sprite;
		} else {
			icon.SetActive (false);
			// GetComponent<HorizontalLayoutGroup> ().padding.left = 50;
		}
		originalColor = text.color;
	}
}
