﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Wikitude;
using System.IO;

public class SceneBundleDownloader : MonoBehaviour
{

    private string url;
	private string prefabNameToLoad;
    private WWW www;
	public Transform spawnPos;
	public bool isAr = false;
	public bool isImageTracking = false;
	public StepNavigation stepNavigation;
	public MenuController menuController;
	public GameObject initCanvas;
	public GameObject guiCanvas;
	public UnityEngine.UI.Image imageCompletion;
	public Text textCompletion;
	public GameObject arEntry;
	public TextMeshProUGUI Description;
	public GameObject txtLoading;
	public GameObject btnInitialize;
	public InstantTrackingController instantTrackingController;
	private GameObject gameObjectToLoad;
	public ImageTracker imageTracker;
	public ImageTrackable imageTrackable;

     // Use this for initialization
    void Start()
    {
        LoadBundle();
    }

	private void LoadBundle()
    {
        if (imageTracker != null && ApplicationModel.mapFilePath != null)
        {
            Debug.Log(ApplicationModel.mapFilePath.Replace("\\", ""));
            imageTracker.TargetCollectionResource.TargetPath = ApplicationModel.mapFilePath.Replace("\\", "");
            imageTracker.enabled = true;
        }
        else {
            Debug.LogWarning("unable to load tracker");
        }
			
		gameObjectToLoad = ApplicationModel.gameObjectToLoad;
		ApplicationModel.resetScale ();

		if(initCanvas)
		initCanvas.SetActive (false);
		if(imageCompletion)
		imageCompletion.fillAmount = 0f;
		if(textCompletion)
		textCompletion.text = "0";

		// StartCoroutine (loadLightmap ());
		if (isAr) {						
			if (isImageTracking) {
				//gameObjectToLoad = Instantiate (gameObjectToLoad);
				//gameObjectToLoad.transform.parent = spawnPos;
				imageTrackable.Drawable = gameObjectToLoad;
				Transform model = imageTrackable.Drawable.transform;
				// Transform model = gameObjectToLoad.transform;
				model.eulerAngles = new Vector3(
					model.eulerAngles.x,
					model.eulerAngles.y+180,
					model.eulerAngles.z
				);
				gameObjectToLoad.transform.localScale = new Vector3 (
                    gameObjectToLoad.transform.localScale.x * 1.7f,
                    gameObjectToLoad.transform.localScale.y * 1.7f,
                    gameObjectToLoad.transform.localScale.z * 1.7f
                );

			} else {
				gameObjectToLoad.transform.localScale = new Vector3 (
					gameObjectToLoad.transform.localScale.x / 5,
					gameObjectToLoad.transform.localScale.y / 5,
					gameObjectToLoad.transform.localScale.z / 5
				);
				gameObjectToLoad.AddComponent<BoxCollider> ();
				instantTrackingController.addModel (gameObjectToLoad);
				txtLoading.SetActive (false);
				btnInitialize.SetActive (true);
			}
            loadLightmap();
            //arAnimationController arAnimationController = gameObjectToLoad.GetComponent<arAnimationController> ();
            //if(arAnimationController==null)
            //	arAnimationController = gameObjectToLoad.AddComponent<arAnimationController> ();
            //if (stepNavigation)
              //   stepNavigation.animator = imageTrackable.gameObject.GetComponentInChildren<Animator>();
                //stepNavigation.animator = FindObjectOfType(typeof(Animator)) as Animator;
            if (menuController) {
                menuController.loadedObject = gameObjectToLoad;
                menuController.InitMenu();
            }               
        } else {
			gameObjectToLoad = Instantiate (gameObjectToLoad);
            gameObjectToLoad.transform.localPosition = new Vector3(
                gameObjectToLoad.transform.localPosition.x,
                0f,
                gameObjectToLoad.transform.localPosition.z);
            menuController.loadedObject = gameObjectToLoad;
            loadLightmap ();
			if(stepNavigation)
			stepNavigation.animator =  FindObjectOfType(typeof(Animator)) as Animator;
            if (menuController)
                menuController.InitMenu();
        }
        if(arEntry)
        arEntry.SetActive(true);

    }

	// IEnumerator loadLightmap(){
	private void loadLightmap(){
		LightmapParams.RestoreAll (LightmapsMode.CombinedDirectional, true, true);
        reloadShaders();
	}

	private void reloadShaders(){
		var materials =  ApplicationModel.bundle.LoadAllAssets<Material>();
		foreach(Material m in materials)
		{
            var shaderName = m.shader.name;
            int renderQueue = m.renderQueue;
            var newShader = Shader.Find(shaderName);
            if (newShader != null)
            {
                m.shader = newShader;
                m.renderQueue = renderQueue;
            }
            else
            {
                Debug.LogWarning("unable to refresh shader: " + shaderName + " in material " + m.name);
            }
        }


        // MeshRenderer[] renderers = (MeshRenderer[])FindObjectsOfType (typeof(MeshRenderer));
        List<MeshRenderer> renderers = new List<MeshRenderer>(gameObjectToLoad.GetComponentsInChildren<MeshRenderer>());

		if (renderers.Count > 0){
			foreach (MeshRenderer renderer in renderers){
                for (int i = 0; i < renderer.sharedMaterials.Length; i++)
                {
                    string shaderName = renderer.sharedMaterials[i].shader.name;
                    int renderQueue = renderer.sharedMaterials[i].renderQueue;
                    renderer.sharedMaterials[i].shader = Shader.Find(shaderName);
                    renderer.sharedMaterials[i].renderQueue = renderQueue;
                }
            }
		}

        List<Renderer> renderers2 = new List<Renderer>(gameObjectToLoad.GetComponentsInChildren<Renderer>());

        if (renderers.Count > 0)
        {
            foreach (Renderer renderer in renderers2)
            {
                for (int i = 0; i < renderer.sharedMaterials.Length; i++)
                {
                    if (renderer.sharedMaterials[i] && renderer.sharedMaterials[i].shader) {
                        string shaderName = renderer.sharedMaterials[i].shader.name;
                        int renderQueue = renderer.sharedMaterials[i].renderQueue;
                        renderer.sharedMaterials[i].shader = Shader.Find(shaderName);
                        renderer.sharedMaterials[i].renderQueue = renderQueue;
                    }
                }
            }
        }
    }
}
