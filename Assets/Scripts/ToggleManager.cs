﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleManager : MonoBehaviour {

	private ToggleController toggleController;
	private Toggle toggle;
	public string type;

	// Use this for initialization
	void Start () {
		toggle = GetComponent<Toggle> ();
		toggle.onValueChanged.AddListener (eventValueChanged);
		toggleController = (ToggleController)FindObjectOfType(typeof(ToggleController));
	}
	
	public void eventValueChanged(bool enabled){
		toggleController.toggle (type);
	}
}
