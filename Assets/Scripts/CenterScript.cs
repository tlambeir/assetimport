﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class CenterScript : MonoBehaviour {

	public LeanCameraZoomSmooth cameraZoom;
	public LeanCameraZoomSmooth cameraZoomUI;
	public SmoothOrbitCam SmoothOrbitCam;
	public Transform mainCameraTransform;
	public LeanCameraMoveSmooth leanCameraMoveSmooth;
	private float originalZoom;
	private float originalZoomUI;
	private Vector3 originalposition;
	public Launch_Animation animationLauncher;

	// Use this for initialization
	void Start () {
        if (cameraZoom) {
            originalZoom = cameraZoom.Zoom;
            originalZoomUI = cameraZoomUI.Zoom;
        }
        //if(mainCameraTransform)
            //originalposition = mainCameraTransform.position;
    }

	// Update is called once per frame
	public void Reset () {
		
        if (cameraZoom) {
            cameraZoom.Zoom = originalZoom;
            cameraZoomUI.Zoom = originalZoomUI;
        }
        if (mainCameraTransform)
            //mainCameraTransform.position = originalposition;
        if (cameraZoom && mainCameraTransform)
            StartCoroutine (HandleIt ());
	}
	
	private IEnumerator HandleIt()
	{
	    // process pre-yield
	    yield return new WaitForSeconds( .1f );
        // process post-yield
        // SmoothOrbitCam.Reset ();
        SmoothOrbitCam.smoothReset();
        cameraZoom.Zoom = originalZoom;
		cameraZoomUI.Zoom = originalZoomUI;
		//mainCameraTransform.position = originalposition;
	}
}
