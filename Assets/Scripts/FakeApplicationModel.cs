﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeApplicationModel : MonoBehaviour {

    [SerializeField] string url;
    [SerializeField] string model;

	// Use this for initialization
	void Start () {
        ApplicationModel.url = url;
        ApplicationModel.model = model;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
