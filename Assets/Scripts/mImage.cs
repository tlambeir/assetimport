﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mImage : MonoBehaviour {

    public Texture2D image;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnDisable()
    {
        GetComponent<RawImage>().texture = null;
    }

    public void Show()
    {
        GetComponent<RawImage>().texture = image;
    }
}
