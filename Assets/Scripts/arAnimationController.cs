﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arAnimationController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Launch_Animation Launch_Animation = GetComponent<Launch_Animation> ();
		if (Launch_Animation && ApplicationModel.arAnimation != "" && ApplicationModel.stepNavigatonIndex < 1)
			Launch_Animation.triggerAnimation (ApplicationModel.arAnimation);
	}
}
