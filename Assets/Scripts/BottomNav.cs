﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomNav : MonoBehaviour {

	public GameObject panel;
	//animator reference
	private Animator anim;
	//variable for checking if the game is paused 
	public bool isPaused = true;
	// Use this for initialization
	void Start () {
        //get the animator component
        if (panel) {
            anim = panel.GetComponent<Animator>();
            //disable it on start to stop it from playing the default animation
            anim.enabled = false;
        }
	}
		
	public void setActive(bool active = true){
		if(active)
			slideUp();
		else
			slideDown ();
	}

	public void slide(){
		Debug.Log (isPaused);
		if (isPaused == true) {
			slideUp ();
		} else {
			slideDown ();
		}
	}

	//function to pause the game
	public void slideUp(){
		anim.enabled = true;
		//enable the animator component
		//play the Slidein animation
		anim.Play("PanelSlideUp");
		//set the isPaused flag to true to indicate that the game is paused
		isPaused = false;
	}
	//function to unpause the game
	public void slideDown(){
		anim.enabled = true;
		//set the isPaused flag to false to indicate that the game is not paused
		isPaused = true;
		//play the SlideOut animation
		anim.Play("PanelSlideDown");
		//set back the time scale to normal time scale<
	}
}
