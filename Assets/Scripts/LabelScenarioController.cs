﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LabelScenarioController : LabelController
{

    public Text labelText;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(parentTransform.position.x, parentTransform.position.y, parentTransform.position.z);
    }

    public new void setText(string text)
    {
        labelText.text = text;
    }

    public new void init(int number)
    {
        // override
    }
}
