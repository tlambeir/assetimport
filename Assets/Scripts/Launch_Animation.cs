﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launch_Animation : MonoBehaviour {

	private Animator animator;
    private bool started = false;

    // Use this for initialization
    void Start () {
        // animator = GetComponent<Animator> ();
        animator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (started) {
            ApplicationModel.currentAnimationTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
           
        }
           
    }

    private void enableAnimatorGUI() {

    }

    public void triggerAnimation(string name = "Anim_State_1_Trigger"){
        Debug.Log("Anim: " + name);
		if (animator != null) {
            SmoothOrbitCam smoothOrbitCam = FindObjectOfType(typeof(SmoothOrbitCam)) as SmoothOrbitCam;
            if(smoothOrbitCam)
            smoothOrbitCam.smoothReset();
            animator.SetTrigger (name);
            // ApplicationModel.currentAnimationTrigger = name;
           started = true;
        }
    }

	public void setIdle(){
		if(animator!=null)
			animator.ResetTrigger ("Anim_State_3_Trigger");
	}
}
