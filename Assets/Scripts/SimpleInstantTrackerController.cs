﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
// using Wikitude;

public class SimpleInstantTrackerController : MonoBehaviour {
//	public InstantTracker Tracker;
//
//	private float _currentDeviceHeightAboveGround = 1.0f;
//
//	private MoveController _moveController;
//	private GridRenderer _gridRenderer;
//
//	private HashSet<GameObject> _activeModels = new HashSet<GameObject>();
//	private InstantTrackingState _currentState = InstantTrackingState.Initializing;
//	private bool _isTracking = false;
//
//	public HashSet<GameObject> ActiveModels {
//		get { 
//			return _activeModels;
//		}
//	}
//
//	private void Awake() {
//		Application.targetFrameRate = 60;
//
//		_moveController = GetComponent<MoveController>();
//		_gridRenderer = GetComponent<GridRenderer>();
//	}
//	#region Begin Drag
//	public void OnBeginDrag (GameObject modelPrefab) {
//		Tracker.SetState(InstantTrackingState.Tracking);
//		SetSceneActive (true);
//		Debug.Log ("SimpleInstantTrackerController OnBeginDrag()");
//		if (_isTracking) {
//			// Create object
//			Transform model = Instantiate(modelPrefab).transform;
//			_activeModels.Add(model.gameObject);
//			// Set model position at touch position
//			var cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
//			Plane p = new Plane(Vector3.up, Vector3.zero);
//			float enter;
//			if (p.Raycast(cameraRay, out enter)) {
//				model.position = cameraRay.GetPoint(enter);
//			}
//
//			// Set model orientation to face toward the camera
//			Quaternion modelRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(-Camera.main.transform.forward, Vector3.up), Vector3.up);
//			model.rotation = modelRotation;
//
//			_moveController.SetMoveObject(model);
//		}
//	}
//	#endregion
//
//	#region Tracker Events
//	public void OnEnterFieldOfVision(string target) {
//		SetSceneActive(true);
//	}
//
//	public void OnExitFieldOfVision(string target) {
//		SetSceneActive(false);
//	}
//
//	private void SetSceneActive(bool active) {
//		foreach (var model in _activeModels) {
//			model.SetActive(active);
//		}
//
//		_gridRenderer.enabled = active;
//		_isTracking = active;
//	}
//
//	public void OnStateChanged(InstantTrackingState newState) {
//		Tracker.DeviceHeightAboveGround = _currentDeviceHeightAboveGround;
//		_currentState = newState;
//		if (newState == InstantTrackingState.Tracking) {
//			// do nothing
//		} else {
//			foreach (var model in _activeModels) {
//				Destroy(model);
//			}
//			_activeModels.Clear();
//		}
//		_gridRenderer.enabled = true;
//	}
//	#endregion
}
