﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HighlightingSystem;
using TMPro;

public class SelectMesh : MonoBehaviour {

	public MenuController MenuController;
	public Branch root;
	public GameObject newMenuItem;
	public Text	Title;
	public TextMeshProUGUI	Description;
	public Transform verticalLayoutGroup;
	public GameObject DescriptionGO;

	public void select(){
		MenuController.resetHighLights ();
		
            if (root.hasAnimation())
                MenuController.triggerAnimation(root);
            Highlighter Highlighter = Object.FindObjectOfType<Highlighter>();
			Destroy(Highlighter);
			if(root.HasChildren)
				MenuController.reBuildMenu (root,true);
			else {
				MenuController.resetcolors ();
				newMenuItem.GetComponent<txtBtnModel> ().setHighlight();
				Title.text = root.Name;
				Description.text = root.Description;
				LayoutRebuilder.ForceRebuildLayoutImmediate (verticalLayoutGroup as RectTransform);
			}

		GameObject mesh = GameObject.Find (root.mesh);
		MenuController.HighLightMesh(mesh, root);
		if (root.Description == null || root.Description == "") {
			Debug.Log ("no description, hide panel");
            DescriptionGO.SetActive (false);
		} else {
			// Debug.Log ("has description, show panel");
            // DescriptionGO.SetActive(true);
            MenuController.setActive(newMenuItem, root);

        }

       
//		Destroy (GetComponent<MeshCollider> ());
//		Destroy (this);
	}

}
