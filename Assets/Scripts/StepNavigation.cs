﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StepNavigation : MonoBehaviour {

	public List<JSONObject> triggers;
	public Animator animator;
	private int currentIndex = 0;
	public GameObject prevButton;
	public GameObject nextButton;
    string scene;

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void startAnimation(){
		play (currentIndex);
    }

	void Update(){
		if (currentIndex > 0) {
			prevButton.SetActive (true);
		} else {
			prevButton.SetActive (false);
		}
		if (currentIndex < triggers.Count - 1) {
			nextButton.SetActive (true);
		} else {
			nextButton.SetActive (false);
		}
	}



	public void play(int index){
        if (scene == "Scenario")
            ApplicationModel.stepNavigatonIndex = currentIndex;
        animator.SetBool ("isStepAnimation", true);
		Debug.Log ("Play, index:"+index);
        Debug.Log("Trigger:" + triggers[index].GetField("trigger").str);
        SmoothOrbitCam smoothOrbitCam = FindObjectOfType(typeof(SmoothOrbitCam)) as SmoothOrbitCam;
        if (smoothOrbitCam)
            smoothOrbitCam.smoothReset();
        animator.Play (triggers [index].GetField("trigger").str);
        MenuController menuController = FindObjectOfType(typeof(MenuController)) as MenuController;
        menuController.setAndShowDescription(triggers[index].GetField("description").str.Replace("\\n", "\n").Replace("\\", ""));
    }

	public void next(){
		Debug.Log ("next");
		if (currentIndex < triggers.Count) {
			currentIndex++;
			play (currentIndex);
		}
	}

	public void prev(){
		Debug.Log ("prev");
		if (currentIndex > 0) {
			currentIndex--;
			play (currentIndex);
		}
	}

	public void reset(){
        Scene m_Scene = SceneManager.GetActiveScene();
        scene = m_Scene.name;
        if (scene == "Scenario")
        {
            currentIndex = ApplicationModel.stepNavigatonIndex;
            ApplicationModel.stepNavigatonIndex = 0;
        }
        else {
            currentIndex = 0;
        }
		prevButton.SetActive (false);
		nextButton.SetActive (false);
		gameObject.SetActive(false);
		animator.SetBool ("isStepAnimation", false);
	}

}
