﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleController : MonoBehaviour {

	public List<TransparancyManager> components = new List<TransparancyManager>();

	public GameObject[] RackList;
	public GameObject[] AMIAList;
	public GameObject[] ABIAList;
	public GameObject[] ASIAList;

	// Use this for initialization
	void Start () {
		RackList =  GameObject.FindGameObjectsWithTag ("Rack");
		AMIAList =  GameObject.FindGameObjectsWithTag ("AMIA");
		ABIAList =  GameObject.FindGameObjectsWithTag ("ABIA");
		ASIAList =  GameObject.FindGameObjectsWithTag ("ASIA");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void enableRackList(){
		foreach (GameObject taggedComponent in RackList) {
			taggedComponent.GetComponent<TransparancyManager>().setTransparancy (false);
		}
	}

	private void enableAMIAList(){
		foreach (GameObject taggedComponent in AMIAList) {
			taggedComponent.GetComponent<TransparancyManager>().setTransparancy (false);
		}
	}

	private void enableABIAList(){
		foreach (GameObject taggedComponent in ABIAList) {
			taggedComponent.GetComponent<TransparancyManager>().setTransparancy (false);
		}
	}

	private void enableASIAList(){
		foreach (GameObject taggedComponent in ASIAList) {
			taggedComponent.GetComponent<TransparancyManager>().setTransparancy (false);
		}
	}

	private void disableAll(){
		foreach (TransparancyManager component in components) {
			component.setTransparancy (true);
		}
	}

	private void enableAll(){
		foreach (TransparancyManager component in components) {
			component.setTransparancy (false);
		}
	}

	public void toggle(string type){
		switch (type) {
		case "Rack":
			disableAll ();
			enableRackList ();
			break;
		case "AMIA":
			disableAll ();
			enableAMIAList ();
			break;
		case "ABIA":
			disableAll ();
			enableABIAList ();
			break;
		case "ASIA":
			disableAll ();
			enableASIAList ();
			break;
		default:
			enableAll ();
			break;
		}
	}
}	
