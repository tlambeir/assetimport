﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelController : MonoBehaviour
{

    public Transform parentTransform;
    public Vector3 Offset = new Vector3(10, 10, 10);
    public float lineSlant = 1f;
    public float lineWidth = 1f;
    public float labelHeight = 1f;
    public float labelWidth = 5f;
    private float yPos;
    public Color color;
    public Transform labelTextTransform;
    private GameObject line;
    private int number;
    private Camera MainCamera;
    private float cameraTempSize;
    private float originalCamerasize;
    private Vector3 tempScale;
    private Vector3 originalPosition;
    // Use this for initialization
    public void init(int number)
    {
        MainCamera = Camera.main;
        cameraTempSize = MainCamera.orthographicSize;
        tempScale = new Vector3(0.1f,0.1f,0.1f);
        originalCamerasize = 7;
        this.number = number;
        // GetComponent<Renderer>().enabled = false;
        transform.position = new Vector3(parentTransform.position.x + Offset.x, parentTransform.position.y + Offset.y, parentTransform.position.z - Offset.z);
        

        // labelHeight = (number != 0 && (number % 3 == 0 || number % 4 == 0) ? labelHeight * 1.5f : labelHeight);
        if (number == 0)
            yPos = transform.position.y + labelHeight;
        else if (number == 1)
            yPos = transform.position.y - labelHeight;
        else
        {
            if (number % 2 == 0)
                yPos = transform.position.y + labelHeight;
            else
                yPos = transform.position.y - labelHeight;
        }


        Vector3 end = new Vector3(transform.position.x + lineSlant / 4, yPos, transform.position.z);
        if (number % 2 == 0)
            labelTextTransform.position = new Vector3(end.x + 0.1f, end.y + 0.25f, labelTextTransform.position.z);
        else
            labelTextTransform.position = new Vector3(end.x + 0.1f, end.y - 0.50f, labelTextTransform.position.z);

        line = DrawLine(transform.position, end, color);


        //declare it locally, so we can have access anywhere from the script
        TextSize ts;

        //put this on the Start function
        ts = new TextSize(labelTextTransform.GetComponent<TextMesh>());
        // DrawLine(end, new Vector3(end.x + ts.width, end.y, end.z), color);
        rescale();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(parentTransform.position.x + Offset.x, parentTransform.position.y + Offset.y, parentTransform.position.z - Offset.z);
        if(cameraTempSize != MainCamera.orthographicSize)
            rescale();
    }

    private void rescale() {
        if (MainCamera.orthographicSize > 3) {
            //Destroy(line);

            float cameraOffset = MainCamera.orthographicSize / cameraTempSize;
            float lineOffset = MainCamera.orthographicSize / originalCamerasize;

            LineRenderer lr = line.GetComponent<LineRenderer>();

            lr.SetPosition(1, new Vector3(originalPosition.x, originalPosition.y * lineOffset, originalPosition.z));


            gameObject.transform.localScale = tempScale * lineOffset;

            //line = DrawLine(transform.position, end, color);
            cameraTempSize = MainCamera.orthographicSize;
        }
        
        
    }

    public void show()
    {
        GetComponent<Renderer>().enabled = true;
    }
    public void hide()
    {
        GetComponent<Renderer>().enabled = false;
    }

    public GameObject DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0f)
    {
        GameObject myLine = new GameObject("line");
        myLine.layer = 8;
        myLine.tag = "Line";
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.01f * lineWidth;
        lr.endWidth = 0.01f * lineWidth;
        lr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        lr.receiveShadows = false;
        // lr.SetWidth(0.01f*lineWidth, 0.01f*lineWidth);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        originalPosition = lr.GetPosition(1);
        myLine.transform.SetParent(transform);
        if (duration > 0f)
            GameObject.Destroy(myLine, duration);
        return myLine;
    }

    public static float GetWidht(TextMesh mesh)
    {
        float width = 0;
        foreach (char symbol in mesh.text)
        {
            CharacterInfo info;
            if (mesh.font.GetCharacterInfo(symbol, out info))
            {
                width += info.width;
            }
        }
        return width * mesh.characterSize * 0.1f * mesh.transform.lossyScale.x;
    }
    public void setText(string text)
    {
        labelTextTransform.GetComponent<TextMesh>().text = text;
    }
}