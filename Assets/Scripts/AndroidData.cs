﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidData {

	public int id;
	public string url;
	public string model;
	public string scene;
	public string thumb;
	public string title;
	public string description;
	public string structure;
}
