﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ascpectRatioManager : MonoBehaviour {

	public CanvasScaler canvasScaler;

	// Use this for initialization
	void Start () {
		if (DeviceDiagonalSizeInInches () > 6.5f) {
			// is tablet
			canvasScaler.referenceResolution = new Vector2(2600f,canvasScaler.referenceResolution.y);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public static float DeviceDiagonalSizeInInches ()
	{
		 float screenWidth = Screen.width / Screen.dpi;
		 float screenHeight = Screen.height / Screen.dpi;
		 float diagonalInches = Mathf.Sqrt (Mathf.Pow (screenWidth, 2) + Mathf.Pow (screenHeight, 2));	 
		 return diagonalInches;
	}
}
