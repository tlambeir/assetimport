﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftNav : MonoBehaviour {

	public GameObject panel;
	public Sprite backSprite;
	public Sprite burgerSprite;
	public GameObject button;
    public GameObject description;
    //animator reference
    public Animator anim;
	//variable for checking if the game is paused 
	public bool isPaused = true;
    // Use this for initialization
    public Text backtotoptext;
    public bool menuHidden = true;
    void Start () {
		//get the animator component
		//anim = panel.GetComponent<Animator>();
		//disable it on start to stop it from playing the default animation
		anim.enabled = false;
	}

	public void slide(){
		if (isPaused == true && menuHidden == false) {
			slideLeft ();
		} else {
			slideRight ();
		}
	}

	public void setActive(bool active = true){
		if(active)
			slideRight();
		else if(menuHidden == false)
			slideLeft ();
	}

	//function to pause the game
	public void slideRight(){
		anim.enabled = true;
        //enable the animator component
        //play the Slidein animation
        if(description && description.activeSelf)
            anim.Play("descriptionOpenShowMenu");
        else
            anim.Play("descriptionClosedShowMenu");
        //set the isPaused flag to true to indicate that the game is paused
        isPaused = true;
        if(backtotoptext)
        backtotoptext.color = new Color32(18, 65, 145, 255);
        menuHidden = false;

    }
	//function to unpause the game
	public void slideLeft(){
		anim.enabled = true;
		//set the isPaused flag to false to indicate that the game is not paused
		isPaused = false;
        //play the SlideOut animation
        if (description && description.activeSelf)
        {
            Debug.Log("descriptionOpenHideMenu");
            anim.Play("descriptionOpenHideMenu");
        }
        else {
            Debug.Log("descriptionClosedHideMenu");
            anim.Play("descriptionClosedHideMenu");
        }
            
        //set back the time scale to normal time scale
        if(backtotoptext)
        backtotoptext.color = new Color32(255, 255, 255, 255);
        menuHidden = true;
    }

    public void showDescription() {

        if (!isPaused)
        {
            anim.enabled = true;
            Debug.Log("menuClosedShowDescription");
            anim.Play("menuClosedShowDescription");
        }
        else {
            anim.enabled = true;
            Debug.Log("menuOpenShowDescription");
            anim.Play("menuOpenShowDescription");
        }
            
    }

    public void hideDescription()
    {
        if (description.activeSelf) {
            if (!isPaused)
            {
                Debug.Log("menuClosedHideDescription");
                anim.Play("menuClosedHideDescription");
            }
            else
            {
                Debug.Log("menuOpenHideDescription");
                anim.Play("menuOpenHideDescription");
            }
        }

    }
}
