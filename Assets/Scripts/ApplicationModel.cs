﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ApplicationModel {
	static public string url;
	static public string model;
	static public string mapFilePath;
	static public string mapFileName;
	static public JSONObject structure;
	static public AssetBundle bundle;
	static public List<Object> gameobjects;
	static public GameObject gameObjectToLoad;
	static public float oldx,oldy,oldz;
	static public Vector3 oldEulerAngles;
	static public string arAnimation = "";
	static public Object[] clips;
  static public Object[] images;
    static public string jsonString;
    static public bool loadFromScene = false;
    static public string loadFromSceneId;
    static public Branch lastRoot;
    static public string lastMenuItem;
    static public string lastScene;
    static public string lastSceneLoadId;
    static public int stepNavigatonIndex;
    static public float currentAnimationTime;
    static public string currentAnimationState;

    public static void resetScale(){
		if (gameObjectToLoad!=null) {
			gameObjectToLoad.transform.localScale = new Vector3 (
				oldx,
				oldy,
				oldz
			);
			gameObjectToLoad.transform.eulerAngles = oldEulerAngles;
		}
	}
}
