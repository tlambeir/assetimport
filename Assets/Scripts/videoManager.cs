﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Wikitude;

public class videoManager : MonoBehaviour {
	public MeshRenderer targetMaterialRenderer;
    public RenderTexture targetTexture;
    public GameObject button;
	public ImageTracker imageTracker;
	public MenuController menuController;
	public GameObject loadingTextMesh;
    public GameObject vidHolder;
    public VideoPlayer videoPlayer;
    

    private VideoPlayer currentVideoPlayer;

    void Start () {
		if (ApplicationModel.mapFilePath != null) {
			imageTracker.TargetCollectionResource.TargetPath = ApplicationModel.mapFilePath.Replace("\\","");
			imageTracker.enabled = true;
		}
		if(menuController)
			menuController.InitMenu ();
    }

	public void playVideo (string  path){
        button.SetActive(false);
		StartCoroutine (playVideoIEnumerator (path));	
	}

    public void prepareDefaultVideo(string path) {
        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        videoPlayer.waitForFirstFrame = false;
        string url;
        if (!Application.isEditor)
        {
            url = ApplicationModel.url + "/" + path;
        }
        else {
            url = path;
        }
        url = url.Replace("\\", "");
        videoPlayer.url = url;
        videoPlayer.Prepare();
    }

	IEnumerator playVideoIEnumerator (string  path){
		currentVideoPlayer = videoPlayer;

        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
		videoPlayer.waitForFirstFrame = false;

        // parse and assign url to VideoPlayer
        string url = ApplicationModel.url + "/" + path;
		url = url.Replace ("\\", "");
        videoPlayer.url = url;

		videoPlayer.Prepare ();
		loadingTextMesh.SetActive (true);
        Debug.Log("Preparing Video: " + url);
        while (!videoPlayer.isPrepared)
		{
			loadingTextMesh.SetActive (true);	
			yield return null;
		}
		Debug.Log("Done Preparing current Video: " + path);
		loadingTextMesh.SetActive (false);
        videoPlayer.Play ();
    }

    public void onVideoSelected() {
        if (videoPlayer.isPlaying)
            pauze();
        else
            play();
    }
	public void pauze(){
        Debug.Log("pauze");
        button.SetActive(true);
        if (videoPlayer.isPrepared && videoPlayer.isPlaying)
            videoPlayer.Pause();
    }
    public void play()
    {
        Debug.Log("play");
        button.SetActive(false);
        //if(videoPlayer.isPrepared && !videoPlayer.isPlaying)
        videoPlayer.Play();
    }
    public void stop()
    {
        if (videoPlayer.isPrepared && videoPlayer.isPlaying)
            videoPlayer.Stop();
    }
}
