﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportBackScript : MonoBehaviour{

    private SceneController sceneController;

	public void FixedUpdate(){
	  if (Application.platform == RuntimePlatform.Android)
	  {
	   if (Input.GetKey(KeyCode.Escape))
	   {
		//	Debug.Log ("SupportBackScript: Escape");
		Application.Quit();
	   }
	  }
	 }

	public void quitApplication(){
		Debug.Log ("SupportBackScript: quitApplication");
        if (ApplicationModel.lastScene == "Scenario")
        {
            sceneController = GetComponent<SceneController>();
            string oldId = ApplicationModel.loadFromSceneId;
            ApplicationModel.loadFromSceneId = ApplicationModel.lastSceneLoadId;
            ApplicationModel.lastSceneLoadId = oldId;
            sceneController.setNewSceneJsonAndLoadInit();
        }
        else {
            if (Application.platform == RuntimePlatform.Android)
            {
                Application.Quit();
            }
        }
	}
}
