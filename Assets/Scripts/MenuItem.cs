﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class MenuItem {
	private string Title;
	private string Description;
	private MenuItem Parent;
	private List<MenuItem> Children;

	public MenuItem(string title, string description){
		Title = title;
		Description = description;
	}

	public void SetTitle(string title){
		Title = title;
	}
	public string GetTitle(){
		return Title;
	}
	public void SetDescription(string description){
		Description = description;
	}
	public string GetDescription(){
		return Description;
	}

	public void SetParent(MenuItem parent){
		Parent = parent;
	}
	public MenuItem GetParent(){
		return Parent;
	}

	public void SetChildren(List<MenuItem> children){
		Children = children;
	}
	public List<MenuItem> GetChildren(){
		return Children;
	}
	public void AddChild(MenuItem child){
		Children.Add (child);
	}
}
