﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wikitude;

public class ScenarioController : SampleController
{
    public Text TrackingQualityText;
    public WikitudeCamera Camera;
    public ImageTracker Tracker;
    public ImageTrackable imageTrackable;
    public StepNavigation stepNavigation;
    public bool extendedTrackingEnabled = false;
    public bool ContinuousAutoExposeEnabled = false;
    public bool ContinuousAutoFocusEnabled = false;
    private List<LabelScenarioController> labelControllers = new List<LabelScenarioController>();

    public void OnSDKInitialized()
    {
        TrackingQualityText.text = Camera.MaxZoomLevel.ToString();
    }

    public void OnTest() {
    }

    public void OnExtendedTrackingQualityChanged(ImageTarget target, ExtendedTrackingQuality oldQuality, ExtendedTrackingQuality newQuality)
    {
        switch (newQuality)
        {
            case ExtendedTrackingQuality.Bad:
                TrackingQualityText.text = "Bad";
                //TrackingQualityBackground.color = Color.red;
                break;
            case ExtendedTrackingQuality.Average:
                TrackingQualityText.text = "Average";
                //TrackingQualityBackground.color = Color.yellow;
                break;
            case ExtendedTrackingQuality.Good:
                TrackingQualityText.text = "Good";
                //TrackingQualityBackground.color = Color.green;
                break;
            default:
                break;
        }
    }

    // Tracker events
    public override void OnTargetsLoaded()
    {

    }

    // Trackable events
    public virtual void OnImageRecognized()
    {
        foreach (LabelScenarioController labelController in labelControllers.ToArray()) {
            labelController.gameObject.SetActive(true);
        }
       
        if (stepNavigation)
        {
            Animator[] Animators = imageTrackable.GetComponentsInChildren<Animator>();

            foreach (Animator animator in Animators)
                stepNavigation.animator = animator;
            //stepNavigation.animator = imageTrackable.Drawable.GetComponent<Animator>();
        }
        MenuController MenuController = FindObjectOfType<MenuController>();
        MenuController.activateMenuItemFromHistory();

    }

    public void setLabelControllers(List<LabelScenarioController> labelControllers) {
        this.labelControllers = labelControllers;
    }

    public virtual void OnImageLost()
    {
        foreach (LabelScenarioController labelController in labelControllers.ToArray())
        {
            labelController.gameObject.SetActive(false);
        }
    }
}
