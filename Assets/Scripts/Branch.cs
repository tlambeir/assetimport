﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;


public class Branch {
	public string Name { get; set; }
	public string Description { get; set; }
    public string Type { get; set; }
    public string Asset { get; set; }
	public Branch Parent { get; set; }
	public TreeBranches Children { get; set; }
	public Boolean isRoot = false;
	public Sprite iconSprite;
	private bool isAnimation;
	public bool isHide;
	public String animationTrigger;
	public String path;
    public String pdate;
    public String mesh;
    public bool showLabel;
    public bool hidden;
    public String labelPosition;
    public List<JSONObject> steps = new List<JSONObject> ();
	public List<string> hides = new List<string> ();
	public bool isStepAnimation = false;
	public bool isActive = false;
    public string inspectId;
    public bool showDot = false;
    public string index;
    public Vector3 cameraPosition;
    public Vector3 cameraRotation;


    //	internal Branch() {
    //		this.Children = new TreeBranches(this);
    //	} // Branch - Constructor - Overload


	internal Branch(string index, string mesh, string Name, string Description, string Type, string Asset, bool hidden = false, bool showLabel = true, String labelPosition = "left", bool isAnimation = false, String animationTrigger = "", String path = "", String pdate = "", string inspectId = "", JSONObject camera = null, bool showDot = false)
    {
        this.Name = Name;
        this.Description = Description;
        this.Children = new TreeBranches(this);
        this.isAnimation = isAnimation;
        this.animationTrigger = animationTrigger;
        this.showLabel = showLabel;
        this.labelPosition = labelPosition;
        this.mesh = mesh;
        this.Type = Type;
        this.Asset = Asset;
		this.path = path;
        this.pdate = pdate;
        this.inspectId = inspectId;
        this.index = index;
        this.hidden = hidden;
        this.showDot = showDot;
        if (camera) {
            this.cameraPosition = new Vector3(
                                camera.GetField("position").GetField("x").f,
                                 camera.GetField("position").GetField("y").f,
                                  camera.GetField("position").GetField("z").f
                              );
            this.cameraRotation = new Vector3(
                                camera.GetField("rotation").GetField("x").f,
                                 camera.GetField("rotation").GetField("y").f,
                                  camera.GetField("rotation").GetField("z").f
                              );
        }        
    }

    internal Branch(string index, string mesh, string Name, string Description,bool showLabel = true, String labelPosition = "left", bool isAnimation = false, String animationTrigger = "") {
		this.Name = Name;
		this.Description = Description;
		this.Children = new TreeBranches(this);
		this.isAnimation = isAnimation;
		this.animationTrigger = animationTrigger;
		this.showLabel = showLabel;
        this.labelPosition = labelPosition;
        this.mesh = mesh;
        this.index = index;
    } // Branch - Constructor - Overload

	internal Branch(string index, string Name, string Description, TreeBranches Children) {
		this.Name = Name;
		this.Description = Description;
		this.Children = Children;
        this.index = index;

        this.Children.ToList().ForEach(delegate(Branch branch) {
			branch.Parent = this;
		});
	} // Branch - Constructor - Overload

	public bool hasAnimation(){
		return isAnimation;
	}

	public void setIsStepAnimation(bool isStepAnimation = true){
		this.isStepAnimation = isStepAnimation;
	}

	public void setIsHide(bool ishide = true){
		this.isHide = ishide;
	}

	/// <summary>
	/// Returns a boolean indicating if the given Branch has any child Branches.
	/// </summary>
	public bool HasChildren {
		get { return this.Children.Count > 0; }
	} // HasChildren - Property - ReadOnly

	/// <summary>
	/// Gets the path from the oldest ancestor to the current Branch.
	/// </summary>
	public string Path {
		get {
			string Result = "";

			Branch parent = this;
			while (parent != null) {
				Result = string.Format("{0}/{1}", parent.Name, Result);
				parent = parent.Parent;
			} // while stepping up the tree

			return IsNullOrWhiteSpace(Result) ? "" : Result.Substring(0, Result.Length - 1);
		} // get
	} // Path - Property - ReadOnly

	public static bool IsNullOrWhiteSpace(string value)
	{
		if (value != null)
		{
			for (int i = 0; i < value.Length; i++)
			{
				if (!char.IsWhiteSpace(value[i]))
				{
					return false;
				}
			}
		}
		return true;
	}

	public void setIconSprite( Sprite sprite){
		iconSprite = sprite;
	}
	public Sprite getIconSprite(){
		return iconSprite;
	}
}