﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTouch : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		  

//			foreach (Touch touch in Input.touches) {
//				Ray ray = Camera.main.ScreenPointToRay(touch.position);

			if ( Input.GetMouseButtonDown (0)){ 
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
				RaycastHit hit;
				if (Physics.Raycast(ray,out hit)) {
					SelectMesh selectedMesh = hit.transform.GetComponent<SelectMesh>();
					if (selectedMesh) {
						selectedMesh.select ();
				}
			}
        }
	}
}
