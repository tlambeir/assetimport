﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HighlightingSystem;
using Lean.Touch;
using TMPro;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    private List<LabelScenarioController> labelControllers = new List<LabelScenarioController>();
    public Sprite tutorialSprite;
    public Sprite componentsSprite;
    public Sprite backSprite;
    public Text Title;
    public TextMeshProUGUI Description;
    public GameObject DescriptionGO;
    public Branch root;
    public GameObject txtBtn;
    public GameObject MenuItemDownBtn;
    public LeftNav LeftNav;
    public Transform verticalLayoutGroup;
    private List<GameObject> activeMenuItems = new List<GameObject>();
    public Launch_Animation animationLauncher;
    public CenterScript CenterScript;
    public GameObject labelPreFab;
    public GameObject dotPreFab;
    public GameObject labelPreFabRight;
    public Color highlightColor;
    public StepNavigation StepNavigation;
    public GameObject contentRenderer;
    public TextMeshProUGUI txtModelName;
    public GameObject backToTopMenuItem;
    public layoutClickManager layoutClickManager;
    public descriptionmanager descriptionmanager;
    public GameObject loadedObject;

    private List<GameObject> hiddenGos = new List<GameObject>();
    private bool isDefaultVidInit = false;
    private string scene;

    TreeBranches parseMenu(JSONObject item, Branch parent) {
        TreeBranches Children = new TreeBranches();
        int i = 0;
        foreach (JSONObject child in item.list) {
            Branch branch = new Branch(
                parent.index + "-" + i.ToString(),
                (child.HasField("mesh") ? child.GetField("mesh").str : ""),
                (child.HasField("name") ? child.GetField("name").str : "").Replace("\\", ""),
                (child.HasField("Description") ? child.GetField("Description").str : "").Replace("\\n", "\n").Replace("\\", ""),
                (child.HasField("type") ? child.GetField("type").str : "").Replace("\\", ""),
                (child.HasField("asset") ? child.GetField("asset").str : "").Replace("\\", ""),
                (child.HasField("hidden") ? child.GetField("hidden").b : false),
                (child.HasField("showLabel") ? child.GetField("showLabel").b : true),
                (child.HasField("labelPosition") ? child.GetField("labelPosition").str : ""),
                (child.HasField("isAnimation") ? child.GetField("isAnimation").b : false),
                (child.HasField("animationTrigger") ? child.GetField("animationTrigger").str : ""),
                (child.HasField("path") ? child.GetField("path").str : ""),
                (child.HasField("pdate") ? child.GetField("pdate").str : ""),
                (child.HasField("inspectId") ? child.GetField("inspectId").str : ""),
                (child.HasField("camera") ? child.GetField("camera") : null),
                (child.HasField("showDot") ? child.GetField("showDot").b : false)
            );
            i++;
            if (child.HasField("steps") && child.GetField("steps").list.Count > 0) {
                foreach (JSONObject step in child.GetField("steps").list) {
                    branch.steps.Add(step);
                }
                branch.setIsStepAnimation();
            }
            if (child.HasField("hide") && child.GetField("hide").list.Count > 0) {
                foreach (JSONObject hide in child.GetField("hide").list) {
                    branch.hides.Add(hide.str);
                }
                branch.setIsHide();
            }
            if (child.GetField("name").ToString().Trim('"') == "Components") {
                branch.setIconSprite(componentsSprite);
            }
            if (child.GetField("name").ToString().Trim('"') == "Tutorials") {
                branch.setIconSprite(tutorialSprite);
            }
            if (child.HasField("Children")) {
                branch.Children = parseMenu(child.GetField("Children"), branch);
            }

            Children.Add(branch);
            branch.Parent = parent;
        }
        return Children;
    }

    // Use this for initialization
    public void InitMenu() {
        Scene m_Scene = SceneManager.GetActiveScene();
        scene = m_Scene.name;
        if (ApplicationModel.structure != null && ApplicationModel.structure.list.Count > 0) {
            int i = 0;
            foreach (JSONObject item in ApplicationModel.structure.list) {
                root = new Branch(i.ToString(), "", item.GetField("name").str, (item.HasField("Description") ? item.GetField("Description").str.Replace("\\n", "\n").Replace("\\", "") : ""));
                i++;
                root.Children = parseMenu(item.GetField("Children"), root);
                root.isRoot = true;
            }
            if (ApplicationModel.lastRoot != null && scene == "Scenario")
            {
                reBuildMenu(ApplicationModel.lastRoot, false);
            }
            else
                reBuildMenu(root, false);
        }

    }

    public void activateMenuItemFromHistory() {
        foreach (GameObject menuitem in activeMenuItems)
        {
            txtBtnModel txtBtnModel = menuitem.GetComponent<txtBtnModel>();
            if (txtBtnModel && txtBtnModel.index == ApplicationModel.lastMenuItem)
            {
                Debug.Log("activate last menu item");
                // setActive(menuitem, ApplicationModel.lastRoot);
                menuitem.GetComponent<Button>().onClick.Invoke();
            }
        }
    }

    public void triggerAnimation(Branch root) {
        // LeftNav.hideDescription();
        // LeftNav.setActive(false);
        animationLauncher = FindObjectOfType(typeof(Launch_Animation)) as Launch_Animation;
        animationLauncher.triggerAnimation(root.animationTrigger);
    }

    public void clearLabels() {
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("Label"))
        {
            GameObject.Destroy(fooObj);
        }
        foreach (LabelScenarioController labelController in labelControllers.ToArray())
        {
            labelControllers.Remove(labelController);
        }
        ScenarioController scenarioController = FindObjectOfType(typeof(ScenarioController)) as ScenarioController;
        if (labelControllers.Count > 0)
            scenarioController.setLabelControllers(labelControllers);
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("Line"))
        {
            GameObject.Destroy(fooObj);
        }
    }

    public void reBuildMenu(Branch root, bool isClick) {
        if (scene == "Scenario")
            ApplicationModel.lastRoot = root;
        clearLabels();
        resetMeshSelection();
        resetHighLights();
        if (backToTopMenuItem != null)
            backToTopMenuItem.SetActive(false);
        if (Title != null) {
            Title.supportRichText = true;
            Title.text = root.Name;
        }
        if (Description != null) {
            Description.text = root.Description;
        }
        if (descriptionmanager != null)
        {
            descriptionmanager.loadId = root.inspectId;
        }
        if (verticalLayoutGroup != null) {
            LayoutRebuilder.ForceRebuildLayoutImmediate(verticalLayoutGroup as RectTransform);
        }
        foreach (GameObject activeMenuItem in activeMenuItems) {
            GameObject.Destroy(activeMenuItem);
        }
        buildMenu(root, 0, 1, 0);
    }

    public void resetcolors() {
        txtBtnModel[] alltxtBtnModels = Object.FindObjectsOfType<txtBtnModel>();
        foreach (txtBtnModel alltxtBtnModel in alltxtBtnModels)
            alltxtBtnModel.resetColor();
    }

    public void resetMeshSelection() {
        SelectMesh[] SelectMeshes = Object.FindObjectsOfType<SelectMesh>();
        foreach (SelectMesh SelectMesh in SelectMeshes) {
            Destroy(SelectMesh.GetComponent<MeshCollider>());
            Destroy(SelectMesh);
        }
    }

    public void resetHighLights() {
        Highlighter[] Highlighters = Object.FindObjectsOfType<Highlighter>();
        foreach (Highlighter Highlighter in Highlighters) {
            // Destroy (Highlighter);
            Highlighter.enabled = false;
        }
    }

    public void resetHidden() {
        foreach (GameObject hide in hiddenGos) {
            hide.SetActive(true);
        }
        hiddenGos.Clear();
    }

    public void backToTop(GameObject mesh, Branch root) {
        resetHidden();
        StepNavigation.reset();
        Debug.Log("Anim_State_3_Trigger");
        animationLauncher = FindObjectOfType(typeof(Launch_Animation)) as Launch_Animation;
        animationLauncher.triggerAnimation("Anim_State_3_Trigger");
        reBuildMenu(root.Parent, true);

        if (root.Parent.Description == null || root.Parent.Description == "") {
            LeftNav.hideDescription();

        } 
        if (mesh) {
            GameObject parentMesh = GameObject.Find(root.Parent.mesh);
            HighLightMesh(parentMesh, root.Parent);
        }
    }

    public void setActive(GameObject newMenuItem, Branch root) {
        Debug.Log("setting active");
        if (scene == "Scenario")
            ApplicationModel.lastMenuItem = newMenuItem.GetComponent<txtBtnModel>().index;
        resetcolors();
        newMenuItem.GetComponent<txtBtnModel>().setHighlight();

        // access dot here?
        txtBtnModel txtBtnModel = newMenuItem.GetComponent<txtBtnModel>();
        if (txtBtnModel && txtBtnModel.dot)
            txtBtnModel.dot.setActive();

        if (Title != null)
            Title.text = root.Name;
        StartCoroutine(menuAnimations(root));
        if (descriptionmanager != null)
        {
            descriptionmanager.loadId = root.inspectId;
        }

        if (verticalLayoutGroup != null)
            LayoutRebuilder.ForceRebuildLayoutImmediate(verticalLayoutGroup as RectTransform);
    }

    IEnumerator menuAnimations(Branch root)
    {
        if (Description != null && root.Description != null && root.Description != "")
        {
            setAndShowDescription(root.Description);
        }
        yield return new WaitForSeconds(0.7f);
        if (!root.HasChildren)
        {
            LeftNav.setActive(false);
        }
    }

    public void setAndShowDescription(string desc) {
        Description.text = desc;
        LeftNav.showDescription();
    }

    public GameObject FindObject(string name)
    {
        if (name == null)
            return null;
        if (loadedObject) {
            Transform[] trs = loadedObject.GetComponentsInChildren<Transform>(true);
            foreach (Transform t in trs)
            {
                if (t.name == name)
                {
                    return t.gameObject;
                }
            }
        }
        return null;
    }


    public void buildMenu(Branch root, int currentDepth, int maxDepth, int index){
        System.DateTime pdateTime = System.DateTime.Now;
        if (root.pdate != null && root.pdate != "") {
            Debug.Log(root.pdate);
            pdateTime = System.DateTime.ParseExact(root.pdate.Replace("\\", ""), "MM/dd/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
        }
        if (root.pdate == null || (root.pdate != null && pdateTime <= System.DateTime.Now))
        {
            resetcolors();           
       
            if (!root.isRoot && currentDepth > 0)
            {
                GameObject newMenuItem = new GameObject();
                if (!root.hidden)
                    newMenuItem = Instantiate(txtBtn, transform);
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
                if (!root.hidden) {
                    txtBtnModel txtBtnModel = newMenuItem.GetComponent<txtBtnModel>();
                    txtBtnModel.text.text = root.Name;
                    txtBtnModel.index = root.index;
                }

                Debug.Log(root.Name);
                Debug.Log(root.mesh);
                // GameObject mesh = GameObject.Find(root.mesh);
                GameObject mesh = FindObject(root.mesh);
                if (mesh != null)
                {
                    Debug.Log("mesh found");
                }

                    videoManager mVideoManager = FindObjectOfType(typeof(videoManager)) as videoManager;
                if (mVideoManager != null && root.path != null && isDefaultVidInit == false)
                {
                    mVideoManager.prepareDefaultVideo(root.path);
                    isDefaultVidInit = true;
                }

                if (currentDepth == 0)
                {
                    newMenuItem.GetComponent<txtBtnModel>().setSprite(null, false, true);

                }
                else
                {
                    if (mesh != null)
                    {
                        if (root.showDot)
                        {
                            GameObject newDot;
                            Scene scene = SceneManager.GetActiveScene();
                            newDot = Instantiate(dotPreFab);
                            DotController dotController = newDot.GetComponent<DotController>();
                            dotController.parentTransform = mesh.GetComponent<Transform>();
                            dotController.setSprite(root.HasChildren);
                            txtBtnModel txtBtnModel = newMenuItem.GetComponent<txtBtnModel>();
                            txtBtnModel.dot = dotController;
                            SelectMesh selectDot = newDot.AddComponent<SelectMesh>();
                            selectDot.MenuController = this;
                            selectDot.root = root;
                            selectDot.newMenuItem = newMenuItem;
                            selectDot.Title = Title;
                            selectDot.Description = Description;
                            selectDot.verticalLayoutGroup = verticalLayoutGroup;
                            selectDot.DescriptionGO = DescriptionGO;
                        }
                        if (root.showLabel)
                        {
                            GameObject newLabel;
                            Scene scene = SceneManager.GetActiveScene();
                            if (root.labelPosition == "right" && scene.name == "Scenario")
                                newLabel = Instantiate(labelPreFabRight);
                            else
                                newLabel = Instantiate(labelPreFab);
                            LabelController labelController;
                            LabelScenarioController labelScenarioController;

                            if (scene.name == "Scenario")
                            {
                                ScenarioController scenarioController = FindObjectOfType(typeof(ScenarioController)) as ScenarioController;
                                labelScenarioController = newLabel.GetComponent<LabelScenarioController>();
                                labelScenarioController.parentTransform = mesh.GetComponent<Transform>();
                                labelScenarioController.setText(root.Name);
                                labelScenarioController.init(index);
                                labelControllers.Add(labelScenarioController);
                                scenarioController.setLabelControllers(labelControllers);
                            }
                            else
                            {
                                labelController = newLabel.GetComponent<LabelController>();
                                labelController.parentTransform = mesh.GetComponent<Transform>();
                                labelController.setText(root.Name);
                                labelController.init(index);
                            }
                        }

                        if (!root.showDot) {
                                                    mesh.AddComponent<MeshCollider>();
                        SelectMesh SelectMesh = mesh.AddComponent<SelectMesh>();
                        SelectMesh.MenuController = this;
                        SelectMesh.root = root;
                        SelectMesh.newMenuItem = newMenuItem;
                        SelectMesh.Title = Title;
                        SelectMesh.Description = Description;
                        SelectMesh.verticalLayoutGroup = verticalLayoutGroup;
                        //SelectMesh.BottomNav = BottomNav;
                        SelectMesh.DescriptionGO = DescriptionGO;
                        }

                    }
                    else {
                        Debug.Log("mesh is null");
                    }

                    if(!root.hidden)
                    newMenuItem.GetComponent<Button>().onClick.AddListener(delegate
                    {

                        mVideoManager = FindObjectOfType(typeof(videoManager)) as videoManager;
                        if (mVideoManager != null)
                        {
                            mVideoManager.playVideo(root.path);
                        }

                        if (StepNavigation != null)
                        {
                            StepNavigation.reset();
                        }
                        resetHighLights();
                        resetHidden();

                        if (DescriptionGO != null && (root.Description == null || root.Description == ""))
                        {
                            Debug.Log("no description, hide panel");
                            LeftNav.hideDescription();

                        }
                        else if (DescriptionGO != null && root.Description != null && root.Description != "")
                        {
                            //LeftNav.showDescription();
                        }

                        if (root.hasAnimation())
                        {
                            root.isActive = false;
                            triggerAnimation(root);
                            if (root.HasChildren)
                            {
                                Debug.Log("has children");
                                reBuildMenu(root, true);
                            }
                            else
                            {
                                Debug.Log("has no children");
                                setActive(newMenuItem, root);
                            }
                            setActive(newMenuItem, root);
                            if (mesh && !root.isHide)
                                HighLightMesh(mesh, root);
                            // if is inspect2 scene, and has camera 
                            setPosition(root);
                        }
                        else if (root.isStepAnimation)
                        {
                            Debug.Log("isStepAnimation");
                            StepNavigation.triggers = root.steps;
                            StepNavigation.gameObject.SetActive(true);
                            setActive(newMenuItem, root);
                            StepNavigation.startAnimation();
                            setPosition(root);
                        }
                        else if (root.isHide && !root.isActive)
                        {
                            Debug.Log("isHide");
                            foreach (string hide in root.hides)
                            {
                                Debug.Log("hide:" + hide);
                                GameObject toHide = GameObject.Find(hide);
                                hiddenGos.Add(toHide);
                                toHide.SetActive(false);
                            }
                            root.isActive = true;
                            setActive(newMenuItem, root);
                        }
                        else
                        {
                            Debug.Log("is hide is false");
                            root.isActive = false;
                            if (root.HasChildren) {
                                Debug.Log("has children");
                                reBuildMenu(root, true);
                            }                
                            else
                            {
                                Debug.Log("has no children");
                                setActive(newMenuItem, root);
                            }
                            if (mesh && !root.isHide)
                                HighLightMesh(mesh, root);
                            animationLauncher = FindObjectOfType(typeof(Launch_Animation)) as Launch_Animation;
                            if (animationLauncher != null)
                                animationLauncher.setIdle();
                        }

                        //Check if selected item has multimedia content, then show the content
                        if (root.Asset != null && root.Asset != "")
                        {
                            contentRenderer.transform.parent.gameObject.SetActive(true);
                            layoutClickManager.enabled = false;
                            StreamVideo streamVideo = contentRenderer.GetComponent<StreamVideo>();
                            mImage imageShow = contentRenderer.GetComponent<mImage>();
                            if (root.Type == "video")
                            {
                                string asset = root.Asset;
                                string clipName = ApplicationModel.clips[0].name;
                                //Find video with same name in downloaded assets, then play it
                                VideoClip clip = System.Array.Find(ApplicationModel.clips, v => v.name == root.Asset) as VideoClip;

                                streamVideo.clip = clip;
                                streamVideo.Play();
                            }
                            else if (root.Type == "image")
                            {

                                //Find image with same name in downloaded assets, then show it
                                Texture2D image = System.Array.Find(ApplicationModel.images, i => i.name == root.Asset) as Texture2D;

                                imageShow.image = image;
                                imageShow.Show();
                            }
                        }
                    });
                    if(!root.hidden)
                    newMenuItem.GetComponent<txtBtnModel>().setSprite(root.getIconSprite());
                }
                if(!root.hidden)
                    activeMenuItems.Add(newMenuItem);
            }
            else
            {
                if(txtModelName != null && root.Name != null && root.Name != null)
                    txtModelName.text = root.Name;
            }
            
            if (root.Children.Count > 0 && currentDepth < maxDepth)
            {
                int childIndex = 0;
                foreach (Branch child in root.Children)
                {
                    buildMenu(child, currentDepth + 1, maxDepth, childIndex);
                    childIndex++;
                }
                if (!root.isRoot)
                {                  
                    GameObject backToTopmesh = GameObject.Find(root.mesh);
                    backToTopMenuItem.GetComponent<Button>().onClick.AddListener(delegate
                    {
                        backToTop(backToTopmesh, root);
                    });
                    backToTopMenuItem.SetActive(true);
                }
                GameObject MenuItemDown = Instantiate(MenuItemDownBtn, transform);
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
                MenuItemDown.GetComponent<Button>().onClick.AddListener(delegate
                {
                    // backToTopMenuItem.SetActive(false);
                    LeftNav.slide();
                });
                activeMenuItems.Add(MenuItemDown);
            }

        }
    }

    private void setPosition(Branch root) {
        if (root.cameraPosition != null && root.cameraRotation != new Vector3(0, 0, 0))
        {
            PlayerMovement mPlayerMovement = FindObjectOfType(typeof(PlayerMovement)) as PlayerMovement;
            mPlayerMovement.setPosition(root.cameraPosition, root.cameraRotation);
        }
    }

	public void HighLightMesh(GameObject mesh, Branch root){
		if (root.Parent!=null) {
			GameObject parentMesh = GameObject.Find (root.Parent.mesh);
			if (parentMesh != null) {
				Highlighter h = parentMesh.GetComponent<Highlighter> ();
				if (h != null)
					h.enabled = false;
			}
				//Destroy(parentMesh.GetComponent<Highlighter>());
		}			
		if (mesh) {
			Highlighter h = mesh.GetComponent<Highlighter>();
			if (h == null) {
				h = mesh.AddComponent<Highlighter>();
			}
			h.enabled = true;
			//h.OnParams (highlightColor);
			h.ConstantOn (highlightColor);
		}

		// h.FlashingOn(new Color(1f, 1f, 0f, 0f), new Color(1f, 1f, 0f, 1f), 2f);
	}
	// Update is called once per frame
	void Update () {
//		if (Description.text != "") {
//			BottomNavGO.SetActive (true);
//			BottomNav.setActive (true);
//		}
//		else {
//			BottomNav.setActive (false);
//			BottomNavGO.SetActive (false);
//		}
	}
}
