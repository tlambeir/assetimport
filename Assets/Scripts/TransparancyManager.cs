﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparancyManager : MonoBehaviour {

	public GameObject transparentAlternative;
	private bool isTransparent = false;

	public void setTransparancy(bool transparent){
		transparentAlternative.SetActive (transparent);
		gameObject.SetActive (transparent ? false : true);
		isTransparent = transparent;
	}

	public void toggleTransparancy(){
		transparentAlternative.SetActive (isTransparent ? false : true);
		gameObject.SetActive (isTransparent ? true : false);
		isTransparent = isTransparent ? false : true;
	}
}
