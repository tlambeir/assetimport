﻿using UnityEngine;
using System.Collections;

public class Test2 : MonoBehaviour {
	private Color[] textColor = {Color.black, Color.blue, Color.cyan, Color.gray, Color.green, Color.grey, Color.magenta, Color.red, Color.white, Color.yellow};
	private int index;
	void Start(){
		// some stuffs
		//textLR = transform.GetComponent<TextMesh>();
	}

	void Update(){
		// some stuffs
		index = Random.Range (0, textColor.Length);
	}

	void OnGUI(){
		// some stuffs
		transform.GetComponent<MeshRenderer>().material.SetColor("_Color", textColor[index]);
	}
}