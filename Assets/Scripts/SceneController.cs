﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

	public GameObject loadingCanvas;
	public string scene;
	public Text txtScene;
    public string loadId = "";

    public void setNewSceneJsonAndLoadInit() {
        ApplicationModel.loadFromScene = true;
        ApplicationModel.loadFromSceneId = loadId;
        Scene m_Scene = SceneManager.GetActiveScene();
        ApplicationModel.lastScene = m_Scene.name;
        LoadScene("Init");
    }

    public void LoadScene(string sceneName){
        if(txtScene)
		    txtScene.text = "Loading " + scene + " viewer";
		loadingCanvas.SetActive (true);
		StartCoroutine( ChangeScene( sceneName));
	}

	public IEnumerator ChangeScene( string sceneName )
	{
		// Display progress bar

		// MUST save a ref to old scene - needed later!
		// Scene oldScene = SceneManager.GetActiveScene();
		// NB: Unity has major bugs in the "by name" call; they recommend you use "by path"
		//  .. which is a workaround to their bugs, but it makes your game hardcoded: if you
		//  .. change your folder structure, your game stops working. It's a nasty hack.
		//  .. Thanks, Unity!

		Scene newScene = SceneManager.GetSceneByName( sceneName );

		// You MUST de-tag your Main-Camera, or Unity's own code crashes
		// ..e.g: nasty bug in Unity's FPS controller: it permanently breaks mouselook!
		// ... if you disable it, your screen will go black / flicker, so detag instead
		//Camera oldMainCamera = Camera.main; // we need this later
		//Camera.main.tag = "Untagged";

		// Start the async load
		// NOTE: Unity does not allow you to pass-in the scene to load as a parameter. WTF?
		AsyncOperation asyncOperation = SceneManager.LoadSceneAsync( sceneName );
		while( ! asyncOperation.isDone ) // note: this will crash at 0.9 if you're not careful
		{
			// 0.9f is a hardcoded magic number inside the SceneManager API
			// ... this is OFFICIAL! Not a hack!
			if( asyncOperation.progress == 0.9f )
			{
				// Unity WILL CORRUPT DATA INTERNALLY AT END OF THIS FRAME
				// ...unless you do several things here.

				// You MUST disable all AudioListeners, or you will get spammed with errors
				// FindObjectOfType<AudioListener>().enabled = false;

				// Because we're at 0.9f, and scene is about to flip-over,
				//  you MUST disable your live cameras
				// oldMainCamera.gameObject.SetActive( false );

				// MUST either delete the old scene,
				//   OR disable every root gameobject
				//				foreach( var go in oldScene.GetRootGameObjects() )
				//					go.SetActive( false );
				//					... or:
				//					Destroy( go );

				yield return null;
			}
		}
		Debug.Log("Scene load is done");
		// Scene has now loaded; but you MUST manually "activate" it
		SceneManager.SetActiveScene( newScene );

	}
}
