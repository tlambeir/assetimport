﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeBranches : IList<Branch> {
	private List<Branch> branches = new List<Branch>();
	private Branch owner;

	public TreeBranches() {
		this.owner = null;
	}

	public TreeBranches(Branch owner) {
		this.owner = owner;
	}

	public void Add(Branch branch) {
		branch.Parent = this.owner;
		this.branches.Add(branch);
	}

	#region Standard IList Method Implementation

	IEnumerator<Branch> IEnumerable<Branch>.GetEnumerator() { return this.branches.GetEnumerator(); }
	System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return this.branches.GetEnumerator(); }
	public int IndexOf(Branch item) { return this.branches.IndexOf(item); }
	public void Insert(int index, Branch item) { this.branches.Insert(index, item); }
	public void RemoveAt(int index) { this.branches.RemoveAt(index); }
	public Branch this[int index] {
		get { return this.branches[index]; }
		set { this.branches[index] = value; }
	}

	public void Clear() { this.branches.Clear(); }
	public bool Contains(Branch item) { return this.branches.Contains(item); }
	public void CopyTo(Branch[] array, int arrayIndex) { this.branches.CopyTo(array, arrayIndex); }
	public int Count { get { return this.branches.Count; } }
	public bool IsReadOnly { get { return this.IsReadOnly; } }
	public bool Remove(Branch item) { return this.branches.Remove(item); }

	#endregion Standard IList Method Implementation
} // TreeBranches - Class