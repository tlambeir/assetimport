﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using System.IO;

public class InitSceneController : MonoBehaviour {

	public UnityEngine.UI.Image imageComp;
	public Text text;
	private string json;
	private bool readyToLoadScene = false;
	private WWW www;
    private string loadId = "27";
	public void Start(){
        if (Application.isEditor) {
            InitSceneLoad("{ \"mapFilePath\":\"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/test.wtc\",\"id\": 32, \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Inspect\", \"model\": \"Nokia_1830_PSI_2T\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/1830_PSI-2T/ANDROID/bundle_nokia_1830_psi_2t_7\", \"mapFileName\": \"Nokia\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/1830_PSI-2T.png\", \"title\": \"Nokia 1830 PSI-2T\", \"duration\": \"0\", \"file_size\": \"4,48\", \"description\": \"\", \"structure\": [ { \"mesh\": \"\", \"name\": \"1830 PSI-2T\", \"Description\": \"\", \"showLabel\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [ { \"mesh\": \"2 redundant fan modules\", \"name\": \"2 redundant fan modules\", \"Description\": \"-3 fans in each fan module \n -Field replaceable \n -LED for visual indication of module status\", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"2 redundant power supply modules\", \"name\": \"2 redundant power supply modules\", \"Description\": \"-2 redundant power supply modules \n -Field replaceable \n AC or DC version available \n -LED for visual indication of module status\", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"C1\", \"name\": \"Client ports\", \"Description\": \"2 ports for 10G/40G\", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"Client ports\", \"name\": \"Client ports\", \"Description\": \"10 combo-ports for 100G(QSFP28) \n The ports support different pluggable optics for different ranges\", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"L1\", \"name\": \"Line ports\", \"Description\": \"4 ports supporting 100G, 200G, or 250G depending on the modulation method used\", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"Management ports\", \"name\": \"Management ports\", \"Description\": \"4x RJ45 interfaces: \n -1x OAMP (Operations, Administration, Maintenance and Provisioning) port \n -1x CIT (Craft Interface Terminal) port \n 2x AUX interfaces for client devices; e.g. for out-of-band management connections \n 1x mini USB interface: \n -Provides a craft interface for serial CLI connectivity \n 1x USB port: \n -Can be used for software upgrading\", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"Status LED\", \"name\": \"Status LED\", \"Description\": \" -Treated as a card level LED \n -Behavior based on the card admin state and the severities of the currently active alarms on the card and the interfaces and modules that it contains \", \"showLabel\": false, \"showDot\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] } ] } ], \"storageType\": \"\" }");
            // InitSceneLoad("{ \"mapFilePath\":\"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/test.wtc\", \"id\": 27, \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Inspect\", \"model\": \"Nokia_WPON_HU\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/WPON/ANDROID/bundle_nokia_wpon_hu6\", \"mapFileName\": \"Nokia\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/NOKIA_WPON_HOU.png\", \"title\": \"Nokia WPON Home Unit\", \"duration\": \"0\", \"file_size\": \"2,70MB\", \"description\": \"Fixed Wireless Access, Wireless PON Home Outdoor Unit (HOU).\", \"structure\": [ { \"mesh\": \"\", \"name\": \"Nokia WPON Home Unit\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": true,\"showDot\": true, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [ { \"mesh\": \"Single Multifunction LED\", \"showDot\": true,\"name\": \"Single Multifunction LED\", \"Description\": \"Red flashing for critical alarms and solid red for major alarms. Green flashing slow for OAM link failure, Green flashing fast for Application startup and solid Green for boot function\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"LED\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"Cable feed\",\"showDot\": true, \"name\": \"Cable feed\", \"Description\": \"Cable feed for the Gigabit Ethernet electrical cable. Also used for providing power to the HOU, using PoE\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"HOU_1\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"Conection point for the ground cable\",\"showDot\": true, \"name\": \"Connection point for the ground cable\", \"Description\": \"Connection point for the ground cable.(throught the installation bracket)\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"HOU_2\", \"steps\": [], \"Children\": [] } ] } ], \"storageType\": \"\" }");
            // InitSceneLoad("{ \"mapFilePath\":\"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/test.wtc\",\"id\": 13, \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Instant\", \"model\": \"tas\", \"target\": \"TAS\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/tas/ANDROID/bundle_tas_4\", \"mapFileName\": \"Nokia\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/TAS.png\", \"title\": \"Telecom Application Server\", \"duration\": \"0\", \"file_size\": \"16.9MB\", \"description\": \"Learn about the Telecom Application Server with this interactive 3D schematic.\", \"structure\": [ { \"mesh\": \"\", \"name\": \"TAS\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [ { \"mesh\": \"\", \"name\": \"Automated schematic\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"Data_Center_Appear\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"\", \"name\": \"Interactive schematic\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [  {\"trigger\":\"Data_Center_Appear\", \"description\":\"desc Data_Center_Appear\"}, {\"trigger\":\"Ntas_Blink_Controllers_Appears\", \"description\":\"desc Ntas_Blink_Controllers_Appears\"}, {\"trigger\":\"NTAS-Appear\", \"description\":\"desc Appear\"}, {\"trigger\":\"VNFC_VM_Blink\", \"description\":\"desc VNFC_VM_Blink\"}, {\"trigger\":\"VNFC_VM_Appears\", \"description\":\"desc VNFC_VM_Appears\"}, {\"trigger\":\"Remaining_Architecture\", \"description\":\"desc Remaining_Architecture\"} ], \"Children\": [] } ] } ], \"storageType\": \"\", \"arAnimation\": \"Data_Center_Appear\" }");
            //InitSceneLoad("[{\"mapFilePath\":\"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/test.wtc\", \"id\": 5, \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Inspect2\", \"model\": \"5g\", \"target\": \"AIRSCALE\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/5g/ANDROID/bundle_5g_10_camera\", \"mapFileName\": \"Nokia\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/AirScale.png\", \"title\": \"5g\", \"duration\": \"0\", \"file_size\": \"27MB\", \"description\": \"Learn about the Nokia 5G system.\", \"structure\": [{ \"mesh\": \"\", \"name\": \"5G\", \"Description\": \"\", \"showLabel\": true, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [{ \"mesh\": \"\", \"name\": \"Scenario 1\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"Scenario_01\", \"steps\": [], \"Children\": [],\"camera\":{ \"position\":{ \"x\":25, \"y\":0, \"z\":38 }, \"rotation\":{ \"x\":27, \"y\":29, \"z\":0 } }},{ \"mesh\": \"\", \"name\": \"Scenario 2\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"Scenario_02\", \"steps\": [], \"Children\": [],\"camera\":{ \"position\":{ \"x\":35, \"y\":41, \"z\":-48 }, \"rotation\":{ \"x\":45, \"y\":-15, \"z\":0 } }}]}]}]");
            //InitSceneLoad("[{\"mapFilePath\":\"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/test.wtc\",\"id\": \"554G\", \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Inspect\", \"model\": \"1830-PSS\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/1830-PSS/ANDROID/bundle_1830-pss_24\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/AirScale.png\", \"title\": \"5G - MULTI-USER MIMO\", \"duration\": \"0\", \"file_size\": \"27MB\", \"description\": \"Discover MULTI-USER MIMO in this 5G Experience! Start a scenario or discover our products in this 5G city.\", \"structure\": [{ \"mesh\": \"\", \"name\": \"5G MULTI-USER MIMO\", \"Description\": \"<color=#124191><b>Welcome to this 5G MULTI-USER MIMO Experience!</b></color>Start a scenario or discover the technology & products in this 5G city.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [{ \"mesh\": \"\", \"name\": \"BACKGROUND INFO\", \"Description\": \"Find out a bit more about MULTI-USER MIMO with this background information.\", \"showLabel\": false, \"steps\": [], \"Children\": [{ \"mesh\": \"\", \"name\": \"What is Massive MIMO?\", \"type\": \"image\", \"asset\": \"i1\", \"Description\": \"<color=#124191><b>What is Massive MIMO?</b></color> \nMassive MIMO (mMIMO) is the extension of traditional MIMO technology to antenna arrays having a large number (>>8) of controllable antenna elements. \n\nA single massive MIMO base station serves multiplexed MIMO users, but number of users should be much smaller than number of base station antennas (usually an order of magnitude). \n\nAs in traditional closed loop MIMO, channel knowledge is needed at the transmitter: \n- TDD can use channel reciprocity \n- FDD uses improved 3GPP Release 13/14 feedback. \n\nBeamforming is a key solution in Massive MIMO, creating a higher frequency range in order to provide sufficient coverage: \n- Beamforming focuses radio energy toward a client through directional signal transmission or reception \n- The beamforming focus increases power, and therefore the signal-to-noise ratio and data rates \n\nThe transmission of data (& control information) to any individual UE is done with the help of dedicated narrowband beams.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"\", \"name\": \"LTE3463 DL MU-MIMO\", \"type\": \"image\", \"asset\": \"i2\", \"Description\": \"<color=#124191><b>LTE3463 DL MU-MIMO</b></color> \nLTE3463 DL MU-MIMO multiplexes users in spatial domain. \n- Transmission on PDSCH is done based on the LTE2666 algorithm \n- Pairing is done based on the predefined grid of beam set \n- Power is split among the multiplexed UEs \n- Significant spectral efficiency gains\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"\", \"name\": \"Analog Beamforming\", \"type\": \"image\", \"asset\": \"i3\", \"Description\": \"<color=#124191><b>Analog Beamforming</b></color> \nMassive MIMO Analog Beamforming key points: \n- For high frequencies (> 6GHz) first prodcuts 29/38GHz \n- 100-200m range from antenna \n- Extend coverage due to high pathloss at high frequencies \n- Beam selected is based on the beam pattern and can change as user moves\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] }, { \"mesh\": \"\", \"name\": \"Digital Beamforming\", \"type\": \"image\", \"asset\": \"i4\", \"Description\": \"<color=#124191><b>Digital Beamforming</b></color> \nMassive MIMO Digital Beamforming key points: \n- For lower frequencies (< 6GHz) first products 3.6/3.8GHz \n- 500m - <1km range, mounted on rooftops, side of buildings \n- Multi-user MIMO to send data to multiple users at the same time using different beams, reduce inter-cell interference \n- Up to 8 UEs that are separated spatially can receive and send data at the same time (during a single transmission interval) \n- Beams can “track” the UE as the UE moves since TRX are integrated to antenna elements and beam is formed in baseband \n- Vertical beamforming supporting vertically spatially separated UEs\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [] } ] },{ \"mesh\": \"\", \"name\": \"DISCOVER\", \"Description\": \"Discover DIGITAL & ANALOG MULTI-USER MIMO with these animated scenarios.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [{ \"mesh\": \"AEQD\", \"name\": \"DIGITAL MULTI-USER MIMO\", \"Description\": \"Starting DIGITAL MULTI-USER MIMO\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [ {\"trigger\":\"Scenario_01\",\"description\":\"<color=#124191><b>AEQD AirScale MAA 64T64R 128AE B43 200W</b></color> \n- Lower frequencies (< 6GHz) first products 3.6/3.8GHz \n- 500m - <1km range, mounted on rooftops, side of buildings \n- Multi-user MIMO to send data to multiple users at the same time using different beams, reduce inter-cell interference \n- Up to 8 UEs that are separated spatially can receive and send data at the same time (during a single transmission interval) \n- Beams can “track” the UE as the UE moves since TRX are integrated to antenna elements and beam is formed in baseband \n- Vertical beamforming supporting vertically spatially separated UEs\"}, {\"trigger\":\"Scenario_01\",\"description\":\"User1 is streaming video content. This user is in a different vertical plane than ground floor users. Digital Multi-User MIMO supports vertical beamforming, making sure this user is being serviced.\"}, {\"trigger\":\"Scenario_01\",\"description\":\"User2 is downloading content with Nokia ontheGO. User1 and User2 are spatially separated & can both have their own beam at the same time. This will increase their capacity.\"}, {\"trigger\":\"Scenario_01\",\"description\":\"User3 is a Smart Car making its route around the park. Beams can “track” the UE as the UE moves since TRX are integrated to antenna elements and beam is formed in baseband. Being spatially separated at the time, it will receive its own beam as well.\"}, {\"trigger\":\"Scenario_01\",\"description\":\"User2 & the Smart Car are too close to each other to receive their own beam. During the time they are close they will share one beam in time intervals.\"}, {\"trigger\":\"Scenario_01\",\"description\":\"User2 & the Smart Car are spatially separated again, each receiving their own beam.\"}, {\"trigger\":\"Scenario_01\",\"description\":\"Multi-User MIMO extends capacity by sending data to multiple users at the same time using different beams. This reduces inter-cell interference. Up to 8 UEs that are separated spatially can receive and send data at the same time (during a single transmission interval).\"}, {\"trigger\":\"Scenario_01\",\"description\":\"DIGITAL MULTI-USER MIMO completed! Try the other scenario or discover the tech & products by going back to the main menu.\"} ], \"Children\": [], \"camera\":{ \"position\":{ \"x\":25, \"y\":0, \"z\":38 }, \"rotation\":{ \"x\":8, \"y\":0, \"z\":15 } } }, { \"mesh\": \"\", \"name\": \"ANALOG MULTI-USER MIMO\", \"Description\": \"\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"Scenario_02\", \"steps\": [ {\"trigger\":\"Antenna presentation\",\"description\":\"<color=#124191><b>AEUA AirScale MAA 2T2R 512AE</b></color> \nScanning for potential users in antenna range. This scanning takes place in a sweeping pattern. \n- For high frequencies (> 6GHz) first products 29/38GHz \n- 100-200m range from antenna \n- Extend coverage due to high pathloss at high frequencies (focused energy of beam to stretch the cell range) \n- One UE scheduled in on transmission time interval with 2 beams (for 2TX/2RX) \n- Beam selected is based on the beam pattern and can change as user moves\"}, {\"trigger\":\"Find User1\",\"description\":\"User1 is picked up by the scanner and receives a beam from the first Antenna.\"}, {\"trigger\":\"Find User2\",\"description\":\"User2 wants to stream a video and is picked up as well. The Antenna will now service both users in time intervals on demand.\"}, {\"trigger\":\"Find User3\",\"description\":\"User3 is about to download learning content with ontheGO and is picked up by the scanner sweep. The Antenna will now service all three users in time intervals on demand.\"}, {\"trigger\":\"A couple seconds after finding all users, explaining beam switching\",\"description\":\"Beam selected is based on the beam pattern and can change as user moves. One UE scheduled in on transmission time interval with 2 beams (for 2TX/2RX).\"}, {\"trigger\":\"Antenna2 start before user beams\",\"description\":\"The second Antenna is starting to service Users.\"}, {\"trigger\":\"Antenna3 & 4 before user beams explaining there's a concert going on\",\"description\":\"A crowd gathered in the park to watch a Nokia concert.\"}, {\"trigger\":\"Antenna3 & 4 in the middle of their random beam to users.\",\"description\":\"Antenna 3 & 4 will now service all the users in range in time intervals on demand.\"}, {\"trigger\":\"Continue last part of animation\",\"description\":\"ANALOG MULTI-USER MIMO completed! Try the other scenario or discover the tech & products by going back to the main menu.\"}, ], \"Children\": [], \"camera\":{ \"position\":{ \"x\":-20, \"y\":0, \"z\":15 }, \"rotation\":{ \"x\":45, \"y\":-15, \"z\":0 } } }] }, ] } ] }]");
            // InitSceneLoad("{ \"mapFilePath\":\"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/end2end.wtc\",\"id\": \"201811231008\", \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Image2\", \"model\": \"end2end\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/end2end/ANDROID/bundle_end2end_201811231024\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/E2E.png\", \"title\": \"5G - END TO END ARCHITECTURE\", \"duration\": \"0\", \"file_size\": \"27MB\", \"description\": \"Learn all about the 5G End to End Architecture with this Augmented Reality experience!\", }");
            //InitSceneLoad("{ \"mapFilePath\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/end2end.wtc\", \"id\": \"201811231008\", \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Image2\", \"model\": \"end2end\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/end2end/ANDROID/bundle_end2end_201811231024\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/E2E.png\", \"title\": \"5G - END TO END ARCHITECTURE\", \"duration\": \"0\", \"file_size\": \"27MB\", \"description\": \"Learn all about the 5G End to End Architecture with this Augmented Reality experience!\", \"structure\": [{ \"mesh\": \"\", \"name\": \"5G End to End Architecture\", \"Description\": \"<color=#124191><b>Welcome to this 5G End to End Architecture experience!</b></color>Discover the scenario or learn about the technology & products in this schematic.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [{ \"mesh\": \"\", \"name\": \"5G End to End Architecture\", \"Description\": \"<color=#124191><b>Welcome to this 5G End to End Architecture experience!</b></color>Discover the scenario or learn about the technology & products in this schematic.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [{ \"mesh\": \"\", \"name\": \"SCENARIO: E2E ARCHITECTURE\", \"Description\": \"Starting END TO END ARCHITECTURE.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [{ \"trigger\": \"Phone appear\", \"description\": \"A user shows up within range. He wants to stream a video.\" }, { \"trigger\": \"Ray\", \"description\": \"The user device makes a connection with the remote radio head that supports 5G NR transmission.\" }, { \"trigger\": \"CPRI\", \"description\": \"CPRI: Common Public Radio Interface provides optical connection between system module and radio unit. This provides control information and data transmission between radio unit and system module.\" }, { \"trigger\": \"S1-C\", \"description\": \"S1-C: Also may be called S1-MME, is the control plane reference point between the LTE eNB and MME. This control plane connection provides core services (authentication, mobility management, bearer management) to the eNB.\" }, { \"trigger\": \"X2-C\", \"description\": \"X2-C: This is the control plane interface between adjacent base stations – either LTE eNB or in this case 5G gNB. This allows communication between base stations for mobility or in this case signaling connection to MME and establishment of bearers for data forwarding between BTS.\" }, { \"trigger\": \"S1-U\", \"description\": \"S1-U: This is a user plane bearer between the SGW and LTE eNB (also used for user data between gNB and SGW). Used for sending user data between UE and packet data network.\" }, { \"trigger\": \"X2-U\", \"description\": \"X2-U: This is a user plane bearer that may be established when needed for mobility or data forwarding.\" }, { \"trigger\": \"CPRI-2\", \"description\": \"CPRI / eCPRI: optical connection between baseband processing in gNB-DU and radio unit.\" }, { \"trigger\": \"F1\", \"description\": \"F1: This is a new 5G 3GPP defined open interface between gNB-CU (central unit) and gNB-DU (distributed unit). The gNB-CU is a logical entity hosting RRC, SDAP, PDCP protocols that controls the operation of one or more gNB-DUs. The gNB-DU is a logical node hosting RLC, MAC, and PHY layers of the gNB, which is controlled by the gNB-CU, and may support one or more cells.\" }, { \"trigger\": \"Edge_cloud_start\", \"description\": \"Edge Cloud: This is one of many distributed data centers in which 5G CU (central unit) and user plane functions are located. They are located closer to the user in order to improve latency and performance.\" }, { \"trigger\": \"Sxa_connection\", \"description\": \"Sxa: Interface between SGW-C (serving gateway – control plane) and SGW-U (serving gateway – user plane). The SGW-C terminates SGW control plane protocols related to session management and UE mobility, it will select the appropriate SGW-U for user plane termination (there may be more SGW-U than SGW-C in the same service area). The SGW-U provides user data packet forwarding and routing and S1-U termination point from eNB or gNB. The SGW-U are distributed and more than one SGW-U may be selected to serve a single UE allowing for reduced latency or service differentiation. Separating user and control plane functions allows for network capacity to be dimensioned independently based on signaling load and data volume.\" }, { \"trigger\": \"SGi_connection\", \"description\": \"SGi: interface to external / private packet data network, or intra operator packet data network (e.g. IMS services).\" }, { \"trigger\": \"S5U_connection\", \"description\": \"S5-U: User plane interface between SGW-U and PGW-U for user data tunneling.\" }, { \"trigger\": \"F1_connection\", \"description\": \"F1: [as described earlier] towards gNB-DU.\" }, { \"trigger\": \"X2_connection\", \"description\": \"X2: [as described earlier] towards LTE eNB.\" }, { \"trigger\": \"S1U_connection\", \"description\": \"S1-U: [as described earlier] towards LTE eNB.\" }, { \"trigger\": \"S1U_2_connection\", \"description\": \"S1-U: [as described earlier] towards 5G gNB-CU.\" }, { \"trigger\": \"Edge_cloud to Central_cloud\", \"description\": \"Central Cloud: This is a central data center in which core functions related to signaling, authentication and management are located.\" }, { \"trigger\": \"S6a_connection\", \"description\": \"S6a: Interface between MME and HSS (home subsciber server) which enables transfer of UE subscription and authentication data for authenticating / authorizing user access.\" }, { \"trigger\": \"S11_connection\", \"description\": \"S11: Control plane Interface between MME and SGW-C used for session and bearer management.\" }, { \"trigger\": \"S5C_connection\", \"description\": \"S5-C: control plane interface between SGW-C and PGW-C used for user plane tunnel management. If control and user plane separation is not used for SGW and PGW then this becomes S5 which includes control and user plane.\" }, { \"trigger\": \"Sxa_02_connection\", \"description\": \"Sxa: [as described earlier]. Coming from SGW-U in the Edge Cloud.\" }, { \"trigger\": \"Sxb2_connection\", \"description\": \"Sxb: [as described earlier]. Coming from PGW-U in the Edge Cloud.\" }], \"Children\": [], \"camera\": { \"position\": { \"x\": 0, \"y\": 15, \"z\": -40 }, \"rotation\": { \"x\": 0, \"y\": 0, \"z\": 0 } } }] }] }] }");
            //InitSceneLoad("{ \"mapFilePath\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/end2end.wtc\",\"id\": \"201811231008\", \"type\": \"unity\", \"apptype\": \"AR & 3D\", \"scene\": \"Inspect\", \"model\": \"end2end\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/end2end/ANDROID/bundle_end2end_201811231024\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/E2E.png\", \"title\": \"5G - END TO END ARCHITECTURE\", \"duration\": \"0\", \"file_size\": \"27MB\", \"description\": \"Learn all about the 5G End to End Architecture with this Augmented Reality experience!\", \"structure\": [{ \"mesh\": \"\", \"name\": \"5G End to End Architecture\", \"Description\": \"<color=#124191><b>Welcome to this 5G End to End Architecture experience!</b></color>\nDiscover the scenario or learn about the technology & products in this schematic.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [], \"Children\": [{ \"mesh\": \"\", \"name\": \"SCENARIO: E2E ARCHITECTURE\", \"Description\": \"Starting END TO END ARCHITECTURE.\", \"showLabel\": false, \"isAnimation\": false, \"animationTrigger\": \"\", \"steps\": [ {\"trigger\":\"Phone appear\",\"description\":\"A user shows up within range. He wants to stream a video.\"}, {\"trigger\":\"Ray\",\"description\":\"The user device makes a connection with the remote radio head that supports 5G NR transmission.\"}, {\"trigger\":\"CPRI\",\"description\":\"CPRI: Common Public Radio Interface provides optical connection between system module and radio unit. This provides control information and data transmission between radio unit and system module.\"}, {\"trigger\":\"S1-C\",\"description\":\"S1-C: Also may be called S1-MME, is the control plane reference point between the LTE eNB and MME. This control plane connection provides core services (authentication, mobility management, bearer management) to the eNB.\"}, {\"trigger\":\"X2-C\",\"description\":\"X2-C: This is the control plane interface between adjacent base stations – either LTE eNB or in this case 5G gNB. This allows communication between base stations for mobility or in this case signaling connection to MME and establishment of bearers for data forwarding between BTS.\"}, {\"trigger\":\"S1-U\",\"description\":\"S1-U: This is a user plane bearer between the SGW and LTE eNB (also used for user data between gNB and SGW). Used for sending user data between UE and packet data network.\"}, {\"trigger\":\"X2-U\",\"description\":\"X2-U: This is a user plane bearer that may be established when needed for mobility or data forwarding.\"}, {\"trigger\":\"CPRI-2\",\"description\":\"CPRI / eCPRI: optical connection between baseband processing in gNB-DU and radio unit.\"}, {\"trigger\":\"F1\",\"description\":\"F1: This is a new 5G 3GPP defined open interface between gNB-CU (central unit) and gNB-DU (distributed unit). The gNB-CU is a logical entity hosting RRC, SDAP, PDCP protocols that controls the operation of one or more gNB-DUs. The gNB-DU is a logical node hosting RLC, MAC, and PHY layers of the gNB, which is controlled by the gNB-CU, and may support one or more cells.\"}, {\"trigger\":\"Edge_cloud_start\",\"description\":\"Edge Cloud: This is one of many distributed data centers in which 5G CU (central unit) and user plane functions are located. They are located closer to the user in order to improve latency and performance.\"}, {\"trigger\":\"Sxa_connection\",\"description\":\"Sxa: Interface between SGW-C (serving gateway – control plane) and SGW-U (serving gateway – user plane). The SGW-C terminates SGW control plane protocols related to session management and UE mobility, it will select the appropriate SGW-U for user plane termination (there may be more SGW-U than SGW-C in the same service area). The SGW-U provides user data packet forwarding and routing and S1-U termination point from eNB or gNB. The SGW-U are distributed and more than one SGW-U may be selected to serve a single UE allowing for reduced latency or service differentiation. Separating user and control plane functions allows for network capacity to be dimensioned independently based on signaling load and data volume.\"}, {\"trigger\":\"SGi_connection\",\"description\":\"SGi: interface to external / private packet data network, or intra operator packet data network (e.g. IMS services).\"}, {\"trigger\":\"S5U_connection\",\"description\":\"S5-U: User plane interface between SGW-U and PGW-U for user data tunneling.\"}, {\"trigger\":\"F1_connection\",\"description\":\"F1: [as described earlier] towards gNB-DU.\"}, {\"trigger\":\"X2_connection\",\"description\":\"X2: [as described earlier] towards LTE eNB.\"}, {\"trigger\":\"S1U_connection\",\"description\":\"S1-U: [as described earlier] towards LTE eNB.\"}, {\"trigger\":\"S1U_2_connection\",\"description\":\"S1-U: [as described earlier] towards 5G gNB-CU.\"}, {\"trigger\":\"Edge_cloud to Central_cloud\",\"description\":\"Central Cloud: This is a central data center in which core functions related to signaling, authentication and management are located.\"}, {\"trigger\":\"S6a_connection\",\"description\":\"S6a: Interface between MME and HSS (home subsciber server) which enables transfer of UE subscription and authentication data for authenticating / authorizing user access.\"}, {\"trigger\":\"S11_connection\",\"description\":\"S11: Control plane Interface between MME and SGW-C used for session and bearer management.\"}, {\"trigger\":\"S5C_connection\",\"description\":\"S5-C: control plane interface between SGW-C and PGW-C used for user plane tunnel management. If control and user plane separation is not used for SGW and PGW then this becomes S5 which includes control and user plane.\"}, {\"trigger\":\"Sxa_02_connection\",\"description\":\"Sxa: [as described earlier]. Coming from SGW-U in the Edge Cloud.\"}, {\"trigger\":\"Sxb2_connection\",\"description\":\"Sxb: [as described earlier]. Coming from PGW-U in the Edge Cloud.\"} ], \"Children\": [], \"camera\":{ \"position\":{ \"x\":0, \"y\":15, \"z\":-40 }, \"rotation\":{ \"x\":0, \"y\":0, \"z\":0 } } }, { \"mesh\": \"Label 002\", \"name\": \"Central Cloud\", \"Description\": \"This is a central data center in which core functions related to signaling, authentication and management are located.\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"central_cloud_roaming\", \"steps\": [], \"Children\": [{ \"mesh\": \"SGWcp_Label\", \"name\": \"SG-W Control plane\", \"Description\": \"This is the control plane function of the SGW and PGW when using control and user plane separation features of the cloud core products (introducted in 3GPP rel. 14). It allows separate of control plane traffic dimensioned from user plane and allows more options and flexibility to be able to select user plane nodes appropriate for the applications service requirements (e.g. latency, use case scenarios).\", \"showLabel\": false, \"steps\": [], \"Children\": [] }, { \"mesh\": \"PGWcp_Label\", \"name\": \"PG-W Control plane\", \"Description\": \"This is the control plane function of the SGW and PGW when using control and user plane separation features of the cloud core products (introducted in 3GPP rel. 14). It allows separate of control plane traffic dimensioned from user plane and allows more options and flexibility to be able to select user plane nodes appropriate for the applications service requirements (e.g. latency, use case scenarios).\", \"showLabel\": false, \"steps\": [], \"Children\": [] }, { \"mesh\": \"HSS_Label\", \"name\": \"HSS\", \"Description\": \"Home subscriber server provides subscriber data and authentication data needed by the MME to authenticate / authorize users and select appropriate services.\", \"showLabel\": false, \"steps\": [], \"Children\": [] }, { \"mesh\": \"MME1_Label\", \"name\": \"MME\", \"Description\": \"This is the mobility management entity that is responsible for authentication / authorization for users on the network as well as user mobility and session management.\", \"showLabel\": false, \"steps\": [], \"Children\": [] }, { \"mesh\": \"PCRF_Label\", \"name\": \"PCRF\", \"Description\": \"This is the policy and charging rule function that provides the PGW with the QoS requirements and charging rules for the bearers established and enforced by the PGW.\", \"showLabel\": false, \"steps\": [], \"Children\": [] } ] }, { \"mesh\": \"Label 2\", \"name\": \"Edge Cloud\", \"Description\": \"This is one of many distributed data centers in which 5G CU (central unit) and user plane functions are located. They are located closer to the user in order to improve latency and performance..\", \"showLabel\": false, \"isAnimation\": true, \"animationTrigger\": \"edge_cloud_roaming\", \"steps\": [], \"Children\": [{ \"mesh\": \"5Gleft_Label\", \"name\": \"5G CU\", \"Description\": \"The 5G central unit (CU) is a logical node hosting RRC, PDCP, SDAP protocols. In Nokia 5G Cloud BTS this is call the Radio Access Controller (RAC) and is a VNF on an Open Stack based cloud infrastructure such as NCIR using NDCS Airframe servers.\", \"showLabel\": false, \"steps\": [], \"Children\": [] }, { \"mesh\": \"PGW_Label\", \"name\": \"PG-W User plane\", \"Description\": \"This is the user plane function of the SGW and PGW when using control and user plane separation features of the cloud core products (introducted in 3GPP rel. 14). It allows for many SGW-U / SGW-U nodes to be distributed closer to the user and provides more options on selecting user plane nodes based on service requirements.\", \"showLabel\": false, \"steps\": [], \"Children\": [] }, { \"mesh\": \"S-GW_Label\", \"name\": \"SG-W User Plane\", \"Description\": \"This is the user plane function of the SGW and PGW when using control and user plane separation features of the cloud core products (introducted in 3GPP rel. 14). It allows for many SGW-U / SGW-U nodes to be distributed closer to the user and provides more options on selecting user plane nodes based on service requirements.\", \"showLabel\": false, \"steps\": [], \"Children\": [] } ] }, { \"mesh\": \"Label 3\", \"name\": \"5G Radio\", \"Description\": \"a remote radio head that supports 5G NR transmission.\", \"showLabel\": false, \"steps\": [], \"Children\": [] },{ \"mesh\": \"Label 3\", \"name\": \"5G Radio\", \"Description\": \"A remote radio head that supports 5G NR transmission.\", \"showLabel\": false, \"steps\": [], \"Children\": [] },{ \"mesh\": \"Label 4\", \"name\": \"LTE Radio\", \"Description\": \"A remote radio head that supports LTE transmission.\", \"showLabel\": false, \"steps\": [], \"Children\": [] },{ \"mesh\": \"Label 5\", \"name\": \"LTE eNodeB\", \"Description\": \"In this deployment the deployment type 5G network architecture option 3x which uses an LTE EPC based core and requires LTE eNB to act as a master node for control plane connection to MME. This is a form of dual connectivty in which the UE will be connected to LTE and 5G NR bearers at the same time.\", \"showLabel\": false, \"steps\": [], \"Children\": [] },{ \"mesh\": \"Label 6\", \"name\": \"5G DU\", \"Description\": \"The 3GPP logical entity name for the distributed unit providing RLC / MAC / PHY processing. This is referred to as the Radio Access Unit (RAU) for the 5G Cloud BTS and is based on AirScale BTS HW.\", \"showLabel\": false, \"steps\": [], \"Children\": [] } ] } ] }");
            //InitSceneLoad("{ \"id\": 26, \"type\": \"unity\", \"apptype\": \"AR Video\", \"scene\": \"Video\", \"model\": \"\", \"url\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/2018-objectives\", \"thumb\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/thumbnails/3.png\", \"title\": \"AR TV\", \"duration\": \"05:12\", \"file_size\": \"15MB\", \"description\": \"AR TV.\", \"structure\": [ { \"name\": \"AR TV\", \"Children\": [ { \"name\": \"week 1\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/video/airframe.mp4\", \"pdate\": \"02/14/2018 00:00\" }, { \"name\": \"week 2\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/video/airframe.mp4\", \"pdate\": \"02/15/2018 00:00\" }, { \"name\": \"week 3\", \"path\": \"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/samples/video/airframe.mp4\", \"pdate\": \"01/16/2018 00:00\" } ] } ], \"storageType\": \"\" }");
        }
    }
			
	public void Update(){
        if (readyToLoadScene)
        {
            bool jsonObjSet = false;
            // load JSON
            if (!json.StartsWith("["))
            {
                json = "[" + json + "]";
            }
            JSONObject jsonArray = new JSONObject(json);
            JSONObject jsonObj = new JSONObject();
            for (int i = 0; i < jsonArray.Count; i++)
            {
                if (!jsonObjSet)
                {
                    jsonObj = loadJson(jsonArray[i]);
                    long id = jsonObj.GetField("id").i;
                    if (id.ToString() == loadId)
                        jsonObjSet = true;
                }

            }
            // load AssetBundle first
            if (jsonObj.GetField("scene").str == "Video")
                loadScene(jsonObj);
            else {
                if(!ApplicationModel.bundle)
                    StartCoroutine(loadAssetBundle(jsonObj));
            }
               
            // loadScene (jsonObj);
            readyToLoadScene = false;
        }
        else if (ApplicationModel.loadFromScene) {
            ApplicationModel.bundle.Unload(false);
            Destroy(ApplicationModel.bundle);
            // DestroyImmediate(ApplicationModel.bundle,true);
            ApplicationModel.loadFromScene = false;
            InitSceneLoadFromScene();            
        }
	}

	private JSONObject loadJson(JSONObject jsonObj)
    {
		if(jsonObj.HasField("path"))
			ApplicationModel.url = jsonObj.GetField ("path").str;

		if(jsonObj.HasField("mapFileName"))
			ApplicationModel.mapFileName = jsonObj.GetField ("mapFileName").str;

		if(jsonObj.HasField("arAnimation"))
			ApplicationModel.arAnimation = jsonObj.GetField ("arAnimation").str;

		if(jsonObj.HasField("mapFilePath"))
			ApplicationModel.mapFilePath = jsonObj.GetField ("mapFilePath").str;

		if(jsonObj.HasField("model"))
			ApplicationModel.model = jsonObj.GetField("model").str;
		else
			Debug.Log ("No model field found");

		if(jsonObj.HasField("structure"))
			ApplicationModel.structure = jsonObj.GetField("structure");
		else
			Debug.Log ("No structure field found");

		return jsonObj;
	}

	IEnumerator loadAssetBundle(JSONObject jsonObj){
		string url = ApplicationModel.url;
		//Begin download
		if(!ApplicationModel.bundle){
            if (Application.isEditor) {
				www = WWW.LoadFromCacheOrDownload (url, 0);
			} else {
				www = WWW.LoadFromCacheOrDownload(url.Replace("\\",""), 0);
			}		
			yield return www;
			ApplicationModel.bundle = www.assetBundle;
		}
		AssetBundleRequest bundleGameObjectRequest = ApplicationModel.bundle.LoadAllAssetsAsync<GameObject>();
		while (!bundleGameObjectRequest.isDone) { // note: this will crash at 0.9 if you're not careful
			//if (bundleGameObjectRequest.progress < 0.9f) {
				imageComp.fillAmount = bundleGameObjectRequest.progress;
				text.text = Mathf.Round ((bundleGameObjectRequest.progress * 100f)).ToString ();
		//	}
			yield return null;
		}
		yield return bundleGameObjectRequest;
		ApplicationModel.gameobjects = new List<Object>(bundleGameObjectRequest.allAssets);
		// List<Object> gameobjects = ApplicationModel.gameobjects;

		        //Load media content
            AssetBundleRequest bundleVideoClipRequest = ApplicationModel.bundle.LoadAllAssetsAsync<VideoClip>();
            yield return bundleVideoClipRequest;
            ApplicationModel.clips = bundleVideoClipRequest.allAssets;
            for(int i = 0; i < ApplicationModel.clips.Length; i++)
            {
               ApplicationModel.clips[i].name = ApplicationModel.clips[i].name.Replace(" (UnityEngine.Videoclip)", "");
            }


            AssetBundleRequest bundleImagesRequest = ApplicationModel.bundle.LoadAllAssetsAsync<Texture2D>();
            yield return bundleImagesRequest;
            ApplicationModel.images = bundleImagesRequest.allAssets;
            for(int i = 0; i <  ApplicationModel.images.Length; i++)
            {
                ApplicationModel.images[i].name = ApplicationModel.images[i].name.Replace(" (UnityEngine.Texture2D)", "");
            }


		// ApplicationModel.bundle.Unload(false);
		www.Dispose();
		loadGameobject ();
		loadScene (jsonObj);
	}

	private void loadGameobject(){
		List<Object> gameobjects = ApplicationModel.gameobjects;
        bool found = false;
		foreach (Object obj in gameobjects)
		{
			string prefabNameToLoad = ApplicationModel.model;
			if (obj.name == prefabNameToLoad) {
                found = true;
				ApplicationModel.gameObjectToLoad = obj as GameObject;

				ApplicationModel.oldx = ApplicationModel.gameObjectToLoad.transform.localScale.x;
				ApplicationModel.oldy = ApplicationModel.gameObjectToLoad.transform.localScale.y;
				ApplicationModel.oldz = ApplicationModel.gameObjectToLoad.transform.localScale.z;
				ApplicationModel.oldEulerAngles = ApplicationModel.gameObjectToLoad.transform.eulerAngles;

				ApplicationModel.gameObjectToLoad.AddComponent (typeof(Launch_Animation));
				// reload gameobject materials
			}
		}
        if (!found) {
            Debug.LogError("Could not locate in Assetbundle: " + ApplicationModel.model);
        }
	}

    public void setLoadId(string id)
    {
        this.loadId = id;
        ApplicationModel.lastSceneLoadId = id;
    }

    public void InitSceneLoadFromScene()
    {
        this.json = ApplicationModel.jsonString;
        this.loadId = ApplicationModel.loadFromSceneId;
        readyToLoadScene = true;
    }

    public void InitSceneLoad(string json){
		this.json = json;
        ApplicationModel.jsonString = json;
        readyToLoadScene = true;
	}

	public void loadScene(JSONObject jsonObj){
		// SceneManager.LoadScene (SceneToLoad);
		if(jsonObj.HasField("scene")){
			imageComp.fillAmount = 0;
			text.text = "0";
			StartCoroutine( ChangeScene( jsonObj.GetField ("scene").str));
		} else
			Debug.Log ("No scene field found");

	}

	public IEnumerator ChangeScene( string sceneName )
	{
		// Display progress bar

			// MUST save a ref to old scene - needed later!
			// Scene oldScene = SceneManager.GetActiveScene();
		// NB: Unity has major bugs in the "by name" call; they recommend you use "by path"
		//  .. which is a workaround to their bugs, but it makes your game hardcoded: if you
		//  .. change your folder structure, your game stops working. It's a nasty hack.
		//  .. Thanks, Unity!
		Scene newScene = SceneManager.GetSceneByName( sceneName );

		// You MUST de-tag your Main-Camera, or Unity's own code crashes
		// ..e.g: nasty bug in Unity's FPS controller: it permanently breaks mouselook!
		// ... if you disable it, your screen will go black / flicker, so detag instead
		Camera oldMainCamera = Camera.main; // we need this later
		Camera.main.tag = "Untagged";

		// Start the async load
		// NOTE: Unity does not allow you to pass-in the scene to load as a parameter. WTF?
		AsyncOperation asyncOperation = SceneManager.LoadSceneAsync( sceneName );
		asyncOperation.allowSceneActivation = false;
		while( ! asyncOperation.isDone ) // note: this will crash at 0.9 if you're not careful
		{
			if (asyncOperation.progress < 0.9f) {
				imageComp.fillAmount = asyncOperation.progress;
				text.text = Mathf.Round((asyncOperation.progress * 100f)).ToString ();
			}
			// 0.9f is a hardcoded magic number inside the SceneManager API
			// ... this is OFFICIAL! Not a hack!
			// Debug.Log("asyncOperation.progress:"+asyncOperation.progress);
			if( asyncOperation.progress == 0.9f)
			{
				yield return new WaitForSeconds (1);
				//Debug.Log("asyncOperation.progress == 0.9f");
				// Unity WILL CORRUPT DATA INTERNALLY AT END OF THIS FRAME
				// ...unless you do several things here.
				text.text = "100";
				imageComp.fillAmount = 1;
				// You MUST disable all AudioListeners, or you will get spammed with errors
				// FindObjectOfType<AudioListener>().enabled = false;


				// Because we're at 0.9f, and scene is about to flip-over,
				//  you MUST disable your live cameras
					// oldMainCamera.gameObject.SetActive( false );

				// MUST either delete the old scene,
				//   OR disable every root gameobject
//				foreach( var go in oldScene.GetRootGameObjects() )
//					go.SetActive( false );
//					... or:
//					Destroy( go );
				asyncOperation.allowSceneActivation = true;
				yield return null;
			}
			yield return null;
		}
		Debug.Log("Scene load is done");
		// Scene has now loaded; but you MUST manually "activate" it
		SceneManager.SetActiveScene( newScene );
	
	}
	
}
